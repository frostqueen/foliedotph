<?php

namespace backend\modules\events\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Events;

/**
 * EventsSearch represents the model behind the search form about `common\models\Events`.
 */
class EventsInSportsSearch extends Events
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'state_id', 'country_name', 'ticketLimit', 'ticketCount'], 'integer'],
            [['name', 'avatar', 'dateStart', 'dateEnd', 'address', 'description', 'thingsToBring', 'reminders', 'inclusions', 'exclusions'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Events::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'dateStart' => $this->dateStart,
            'dateEnd' => $this->dateEnd,
            'city_id' => $this->city_id,
        	'state_id' => $this->state_id,
        	'country_name' => $this->country_name,
            'ticketLimit' => $this->ticketLimit,
            'ticketCount' => $this->ticketCount,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'avatar', $this->avatar])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'thingsToBring', $this->thingsToBring])
            ->andFilterWhere(['like', 'reminders', $this->reminders])
            ->andFilterWhere(['like', 'inclusions', $this->inclusions])
            ->andFilterWhere(['like', 'exclusions', $this->exclusions]);

        return $dataProvider;
    }
}
