<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Events */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sports-box-image col-md-8">
	<div class="box-body">
		<p>
	        <?php if ($model->avatar) : ?>
	            <img src="<?= Yii::getAlias('@storageUrl/folie/events/' . $model->avatar) ?>" class="sports-img">
	        <?php else: ?>
	            <img src="<?= Yii::getAlias('@storageUrl/folie/bg.jpg') ?>" class="sports-img">
	        <?php endif ?>
	    </p>
		<section class="panel-default event-header box">
			<div class="panel-body">
				<?php if ($this->title !== null) {
					echo '<h3 class="sports-header">' .Html::encode($this->title).'</h3>' ;
					/*echo '<h6>'. Html::encode($model->status ? 'Active' : 
					'Inactive').' > '.Html::encode($model->parent ? $model->parent->name : 'Main Category').'</h6>';*/
				} else {
					echo Inflector::camel2words(Inflector::id2camel($this->context->module->id));
					echo ($this->context->module->id !== Yii::$app->id) ? '<small>Module</small>' : '';
				} ?>
				<div class="sports-text">
					<?= HtmlPurifier::process($model->description) ?>
				</div>
			</div>
	    </section>
		
		<section class="panel panel-default">
			<div class="panel-heading">
				Event Details
			</div>			
			<div class="panel-body">
				<?= DetailView::widget([
			        'model' => $model,
			        'attributes' => [
			        	[
			        		'attribute'=> 'dateStart',
			        		'value'=> $model->dateStart,
			        		'format'=> 'Datetime',
			        	],        		
			        	[
			        		'attribute'=> 'dateEnd',
			        		'value'=> $model->dateEnd,
			        		'format'=> 'Datetime',
			        	],
			            'address',
			        	[
			        		'attribute'=> 'city_id',
			        		'value'=> $model->city->name,
			        	],
			        	'state_id',
			        	'country_name',
			            'description:html',
			            'ticketLimit',
			            'ticketCount',
			            'thingsToBring:html',
			            'reminders:html',
			            'inclusions:html',
			            'exclusions:html',
			        ],
			    ]) ?>
			</div>
	    </section>
	    <section class="panel panel-default">
			<div class="panel-heading">
				Event Discussion
			</div>			
			<div class="panel-body">
				The quick brown fox jumps over the lazy dog near the bank of the river. 
				The quick brown fox jumps over the lazy dog near the bank of the river.
				The quick brown fox jumps over the lazy dog near the bank of the river.
				The quick brown fox jumps over the lazy dog near the bank of the river.
			</div>
	    </section>
	</div>
</div>

<?php echo $this->render('_right', ['model' => $model]); ?>


