<?php

/* @var $this yii\web\View */
/* @var $model common\models\Events */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Events',
]) . $events->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Events'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $events->name, 'url' => ['view', 'id' => $events->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>

<div class="sports-box-image col-md-8">
	<section class="panel panel-default">
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'events' => $events,
		    	'city' => $city,
		    	'state' => $state,
		    	'country' => $country,
		    	'ticketCount' => $ticketCount,
		    ]) ?>
		</div>
	</section>
</div>