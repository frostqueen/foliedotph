<?php 

use yii\helpers\Html;

?>
<div class="col-md-4">
	<div class="panel panel-default">
	  	<div class="panel-body">
			Event Organizer
			<?php echo Yii::$app->formatter->format($model->dateStart, 'date'); ?>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-body">
	    	<?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
	        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
	            'class' => 'btn btn-danger',
	            'data' => [
	                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
	                'method' => 'post',
	            ],
	        ]) ?>
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">Attendee</div>
		<div class="panel-body">
	    	The quick brown fox jumps over the lazy dog near the bank of the river.
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">Invite Friends / Photos if event is done</div>
		<div class="panel-body">
	    	The quick brown fox jumps over the lazy dog near the bank of the river.
		</div>
	</div>
	<div class="panel panel-default">
		<div class="panel-heading">Other Upcoming Events of Organizer</div>
		<div class="panel-body">
	    	The quick brown fox jumps over the lazy dog near the bank of the river.
		</div>
		<div class="panel-footer"><a href="">View All</a></div>
	</div>
</div>