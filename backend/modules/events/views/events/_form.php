<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use vova07\imperavi\Widget;
use vova07\fileapi\Widget as Avatar;
use trntv\yii\datetime\DateTimeWidget;
//use kartik\datetime\DateTimePicker;
//use dosamigos\datetimepicker\DateTimePicker;
use dosamigos\selectize\SelectizeTextInput;

/* @var $this yii\web\View */
/* @var $model common\models\Events */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="events-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($events, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($events, 'avatar')->widget(
        Avatar::className(), [
            'settings' => [
                'url' => ['/site/fileapi-upload'],
            ],]) ?>

    <?= $form->field($events, 'dateStart')->widget(DateTimeWidget::className(), [
    		'clientOptions' => [
    			'allowInputToggle' => false,
    			'sideBySide' => true,
    			'locale' => 'en-au',
    		]]) ?>

    <?= $form->field($events, 'dateEnd')->widget(DateTimeWidget::className(), [
    		'clientOptions' => [
    			'allowInputToggle' => false,
    			'sideBySide' => true,
    			'locale' => 'en-au',
    		]]) ?>

    <?= $form->field($events, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($events, 'city_id')->dropDownList($city, ['prompt' => '']) ?>
    
    <?= $form->field($events, 'state_id')->dropDownList($state, ['prompt' => '']) ?>
    
    <?= $form->field($events, 'country_name')->dropDownList($country, ['prompt' => '']) ?>

	<?= $form->field($events, 'description')->widget(Widget::className(), [
        'settings' => [
            'minHeight' => 150,
            'plugins' => [
                'imagemanager',
                'video',
            ],
            'imageManagerJson' => Url::to(['/site/images-get']),
            'imageUpload' => Url::to(['/site/image-upload']),
        ],]) ?>

    <?= $form->field($events, 'ticketLimit')->textInput() ?>

    <?= $form->field($events, 'ticketCount')->radioList($ticketCount) ?>
    
	<?= $form->field($events, 'thingsToBring')->widget(Widget::className(), [
        'settings' => [
            'minHeight' => 150,
            'plugins' => [
                'imagemanager',
                'video',
            ],
            'imageManagerJson' => Url::to(['/site/images-get']),
            'imageUpload' => Url::to(['/site/image-upload']),
        ],
    ]) ?>

	<?= $form->field($events, 'reminders')->widget(Widget::className(), [
        'settings' => [
            'minHeight' => 150,
            'plugins' => [
                'imagemanager',
                'video',
            ],
            'imageManagerJson' => Url::to(['/site/images-get']),
            'imageUpload' => Url::to(['/site/image-upload']),
        ],
    ]) ?>

	<?= $form->field($events, 'inclusions')->widget(Widget::className(), [
        'settings' => [
            'minHeight' => 150,
            'plugins' => [
                'imagemanager',
            ],
            'imageManagerJson' => Url::to(['/site/images-get']),
            'imageUpload' => Url::to(['/site/image-upload']),
        ],
    ]) ?>

    <?= $form->field($events, 'exclusions')->widget(Widget::className(), [
        'settings' => [
            'minHeight' => 150,
            'plugins' => [
                'imagemanager',
            ],
            'imageManagerJson' => Url::to(['/site/images-get']),
            'imageUpload' => Url::to(['/site/image-upload']),
        ],
    ]) ?>
	<?= $form->field($events, 'tagValues')->widget(SelectizeTextInput::className(), [
        'loadUrl' => ['/sports/items'],
        'options' => ['class' => 'form-control'],
        'clientOptions' => [
            'plugins' => ['remove_button'],
            'valueField' => 'name',
            'labelField' => 'name',
            'searchField' => ['name'],
            'create' => false,
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton($events->isNewRecord ? Yii::t('backend', 'Save') : Yii::t('backend', 'Update'), ['class' => $events->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
