<?php

use yii\helpers\Html;

?>

<div class="col-md-4" style="margin-bottom:25px;">
	<?php if ($model->avatar) : ?>
		<?= Html::a(Html::img('@storageUrl/folie/events/'.$model->avatar, ['class' => 'result-img']), ['view', 'id' => $model->id]) ?>
	<?php else: ?>
		<?= Html::a(Html::img('@storageUrl/folie/bg.jpg', ['class' => 'result-img']), ['view', 'id' => $model->id]) ?>
	<?php endif ?>
	<div class="thumb-link">
		<?php echo Yii::$app->formatter->asDate($model->dateStart); ?>
		<?php echo Yii::t('backend', $model->name); ?>
	</div>    
</div>
