<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use trntv\yii\datetime\DateTimeWidget;

/* @var $this yii\web\View */
/* @var $model backend\models\search\EventsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="events-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'name') ?>

	<?= $form->field($model, 'dateStart')->widget(DateTimeWidget::className(), [
    	'clientOptions' => [
    		'allowInputToggle' => false,
    		'sideBySide' => true,
    		'locale' => 'en-au',
    ]]) ?>

    <?= $form->field($model, 'dateEnd')->widget(DateTimeWidget::className(), [
    	'clientOptions' => [
    		'allowInputToggle' => false,
    		'sideBySide' => true,
    		'locale' => 'en-au',
    ]]) ?>
    
    <?php //$form->field($model, 'avatar') ?>

    <?php // echo $form->field($model, 'address') ?>

    <?php // echo $form->field($model, 'city_id') ?>
    
    <?php // echo $form->field($model, 'state_id') ?>
    
    <?php // echo $form->field($model, 'country_id') ?>

    <?php // echo $form->field($model, 'description') ?>

    <?php // echo $form->field($model, 'ticketLimit') ?>

    <?php // echo $form->field($model, 'ticketCount') ?>

    <?php // echo $form->field($model, 'thingsToBring') ?>

    <?php // echo $form->field($model, 'reminders') ?>

    <?php // echo $form->field($model, 'inclusions') ?>

    <?php // echo $form->field($model, 'exclusions') ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('backend', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('backend', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
