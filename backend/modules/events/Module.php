<?php

namespace backend\modules\events;

use yii\base\Module as BaseModule;

/**
 * events module definition class
 */
class Module extends BaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\events\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
