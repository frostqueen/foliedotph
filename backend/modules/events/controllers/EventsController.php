<?php

namespace backend\modules\events\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use backend\modules\events\models\search\EventsSearch;
use common\models\Events;
use common\models\City;
use common\models\State;
use common\models\Country;

/**
 * EventsController implements the CRUD actions for Events model.
 */
class EventsController extends Controller
{
    /**
     * @inheritdoc
     */
	public $layout = '@app/views/layouts/sports-view';
	
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Events models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new EventsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $city = ArrayHelper::map(City::find()->orderBy([
        		'name' => SORT_ASC ])->asArray()->all(), 'id', 'name');
        $state = State::find()->orderBy([
        		'name' => SORT_ASC ])->all();
        $state = ArrayHelper::map($state, 'id', 'name');
        $country = Country::find()->orderBy([
        		'name' => SORT_ASC ])->all();
        $country = ArrayHelper::map($country, 'name', 'name');
        
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        	'city' => $city,
        	'state' => $state,
        	'country' => $country,
        ]);
    }

    /**
     * Displays a single Events model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Events model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $events = new Events();
        $city = City::find()->orderBy([
        		'name' => SORT_ASC ])->all();
        $city = ArrayHelper::map($city, 'id', 'name');
        $state = State::find()->orderBy([
        		'name' => SORT_ASC ])->all();
        $state = ArrayHelper::map($state, 'id', 'name');
        $country = Country::find()->orderBy([
        		'name' => SORT_ASC ])->all();
        $country = ArrayHelper::map($country, 'name', 'name');
        $ticketCount = Events::statuses();
                 
        if ($events->load(Yii::$app->request->post()) && $events->save()) {
            return $this->redirect(['view', 'id' => $events->id]);
        } else {
            return $this->render('create', [
                'events' => $events,
            	'city' => $city,
            	'state' => $state,
            	'country' => $country,
            	'ticketCount' => $ticketCount,
            ]);
        }
    }

    /**
     * Updates an existing Events model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $events = $this->findModel($id);
        $city = City::find()->orderBy([
        		'name' => SORT_ASC ])->all();
        $city = ArrayHelper::map($city, 'id', 'name');
        $state = State::find()->orderBy([
        		'name' => SORT_ASC ])->all();
        $state = ArrayHelper::map($state, 'id', 'name');
        $country = Country::find()->orderBy([
        		'name' => SORT_ASC ])->all();
        $country = ArrayHelper::map($country, 'name', 'name');
        $ticketCount = Events::statuses();
                
        if ($events->load(Yii::$app->request->post()) && $events->save()) {
        	Yii::$app->session->setFlash('success', Yii::t('backend', 'Event details was successfully saved.'));
            return $this->redirect(['view', 'id' => $events->id]);
        } else {
            return $this->render('update', [
                'events' => $events,
            	'city' => $city,
            	'state' => $state,
            	'country' => $country,
            	'ticketCount' => $ticketCount,
            ]);
        }
    }

    /**
     * Deletes an existing Events model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', Yii::t('backend', 'Event was successfully deleted.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the Events model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Events the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Events::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
