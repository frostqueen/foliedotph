<?php

namespace backend\modules\spot\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Spot;

/**
 * SpotSearch represents the model behind the search form about `frontend\models\Spot`.
 */
class SpotSearch extends Spot
{
	public $city;
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'city_id', 'created_at', 'updated_at', 'status'], 'integer'],
            [['name', 'avatar', 'description', 'address', 'city'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Spot::find();
        $query->joinWith(['city']);
        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->sort->attributes['city'] = [
        		// The tables are the ones our relation are configured to
        		// in my case they are prefixed with "tbl_"
        		'asc' => ['city.name' => SORT_ASC],
        		'desc' => ['city.name' => SORT_DESC],
        ];
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'city_id' => $this->city_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'spot.status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'description', $this->description])
        	->andFilterWhere(['like', 'city.name', $this->city])
        	->andFilterWhere(['like', 'spot.name', $this->name])
            ->andFilterWhere(['like', 'address', $this->address]);

        return $dataProvider;
    }
}
