<?php

namespace backend\modules\spot\controllers;

use Yii;
use yii\web\Controller;
use common\models\Spot;
use common\models\City;
use backend\modules\spot\models\search\SpotSearch;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * Default controller for the `spot` module
 */
class SpotController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public $layout = '@app/views/layouts/sports-view';
	
	public function behaviors()
	{
		return [
				'verbs' => [
						'class' => VerbFilter::className(),
						'actions' => [
								'delete' => ['POST'],
						],
				],
		];
	}
	
	/**
	 * Lists all Spot models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new SpotSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
	
		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}
	
	/**
	 * Displays a single Spot model.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionView($id)
	{		
		return $this->render('view', [
			'model' => $this->findModel($id),
		]);
	}
	
	/**
	 * Creates a new Spot model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Spot();
		$city = City::find()->orderBy([
				'name' => SORT_ASC ])->all();
		$city = ArrayHelper::map($city, 'id', 'name');
		$spots_status = Spot::statuses();
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('create', [
				'model' => $model,
				'city' => $city,
				'spots_status' => $spots_status,
			]);
		}
	}
	
	/**
	 * Updates an existing Spot model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		$city = City::find()->orderBy([
				'name' => SORT_ASC ])->all();
		$city = ArrayHelper::map($city, 'id', 'name');
		$spots_status = Spot::statuses();
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['view', 'id' => $model->id]);
		} else {
			return $this->render('update', [
				'model' => $model,
				'city' => $city,
				'spots_status' => $spots_status,
			]);
		}
	}
	
	/**
	 * Deletes an existing Spot model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();
	
		return $this->redirect(['index']);
	}
	
	
	/**
	 * Finds the Char model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Char the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findChar($char_id)
	{
		if (($charModel = Char::findOne($char_id)) !== null) {
			return $charModel;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
	
	/**
	 * Finds the Spot model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Spot the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Spot::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
