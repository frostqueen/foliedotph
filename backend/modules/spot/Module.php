<?php

namespace backend\modules\spot;

use yii\base\Module as BaseModule;

/**
 * spot module definition class
 */
class Module extends BaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\spot\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
