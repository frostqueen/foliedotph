<?php

/* @var $this yii\web\View */
/* @var $model common\models\Spot */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Spot',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Spots'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>

<div class="sports-box-image col-md-8">
	<section class="panel panel-default sports-box box">
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    	'city' => $city,
		    	'spots_status' => $spots_status,
		    ]) ?>
		</div>
	</section>
</div>
