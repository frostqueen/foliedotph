<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ListView;
use yii\widgets\Pjax;
use common\models\Spot;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SpotSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Spots');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="sports-box-image col-md-8">
	<div class="box-body">
		<section class="panel panel-default">
			<div class="panel-heading">Events</div>
			<div class="panel-body">        
				<?php /*Pjax::begin(); ?>    <?= GridView::widget([
			        'dataProvider' => $dataProvider,
			        'filterModel' => $searchModel,
			        'columns' => [
			            //['class' => 'yii\grid\SerialColumn'],
			            'id',
			        	'name',
			        	'avatar',
			            //'description:ntext',
			            //'address',
			        	[
			        		'attribute' => 'city',
			        		'value' => 'city.name'
			        	],
			        	[
			        		'attribute' => 'status',
			        		'format' => 'html',
			        		'value' => function ($model) {
			        		return $model->status ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>';
			        		},
			        		'filter' => [
			        				Spot::STATUS_INACTIVE => Yii::t('backend', 'Inactive'),
			        				Spot::STATUS_ACTIVE => Yii::t('backend', 'Active'),
			        		],
			        	],
			            ['class' => 'yii\grid\ActionColumn'],
			        ],
			    ]); ?>
				
				<?php Pjax::end(); */?>
				<?= ListView::widget([
			        'dataProvider' => $dataProvider,
			        'itemOptions' => ['class' => 'item'],
			        'itemView' => '_thumb',
			    ]) ?>
			</div>
		</section>
	</div>
</div>
<div class="col-md-4">
	<div class="panel panel-default">
		<div class="panel-heading">Search</div>
		<div class="panel-body">
	    	<?php echo $this->render('_search', ['model' => $searchModel]); ?>
		</div>
	</div>
</div>
