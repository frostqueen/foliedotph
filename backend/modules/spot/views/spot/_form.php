<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\helpers\Url;
//use vova07\fileapi\Widget;
use dosamigos\selectize\SelectizeTextInput;


use yii\helpers\ArrayHelper;
use trntv\yii\datetime\DateTimeWidget;
use vova07\imperavi\Widget;

/* @var $this yii\web\View */
/* @var $model common\models\Spot */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="spot-form">

    <?php $form = ActiveForm::begin(); ?>

	<?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    
	<?= $form->field($model, 'avatar')->widget(
        Widget::className(),
        [
            'settings' => [
                'url' => ['/site/fileapi-upload'],
            ],
            /*'crop' => true,
            'cropResizeWidth' => 1057,
            'cropResizeHeight' => 175,*/
        ]
    ) ?>
    
    <?= $form->field($model, 'city_id')->dropDownList($city, ['prompt' => '']) ?>

    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
    
    <?php //= $form->field($model, 'status')->label(Yii::t('backend', 'Status'))->radioList($spots_status) ?>

	<?php /*= $form->field($model, 'sportsValues')->widget(SelectizeTextInput::className(), [	
        'loadUrl' => ['/sports/sports/list'],
        'options' => ['class' => 'form-control'],
        'clientOptions' => [
            'plugins' => ['remove_button'],
            'valueField' => 'name',
            'labelField' => 'name',
            'searchField' => ['name'],
            'create' => false,
        ],
    ]) ;*/
	?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Save') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
