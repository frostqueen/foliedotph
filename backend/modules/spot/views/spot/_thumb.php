<?php
use yii\helpers\Html;
?>
<div class="col-md-3 subcategory-list">

	<?php if ($model->avatar) : ?>
		<?= Html::a(Html::img('@storageUrl/folie/spots/'.$model->avatar, ['class' => 'result-img']), ['view', 'id' => $model->id]) ?>
	<?php else: ?>
		<?= Html::a(Html::img('@storageUrl/folie/bg.jpg', ['class' => 'result-img']), ['view', 'id' => $model->id]) ?>
	<?php endif ?>
	<?= Html::a(Yii::t('backend', $model->name), ['view', 'id' => $model->id], ['class' => '']) ?>
    
</div>
