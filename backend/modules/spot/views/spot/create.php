<?php

/* @var $this yii\web\View */
/* @var $model common\models\Spot */

$this->title = Yii::t('backend', 'Add New Spot');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Spots'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="sports-box-image col-md-8"">
	<section class="panel panel-default">
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    	'city' => $city,
		    	'spots_status' => $spots_status,
		    ]) ?>
		    </div>
	</section>
</div>
