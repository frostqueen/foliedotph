<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;


/* @var $this yii\web\View */
/* @var $model common\models\Spot */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Spots'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="sports-box-image col-md-8">
	<div class="box-body">
		<?php 
			foreach ($model->chars as $chars) {
				echo $chars->name.'<br/>';
			}
		?>
	    <?= DetailView::widget([
	        'model' => $model,
	        'attributes' => [
	        	[
	        		'attribute'=> 'avatar',
	        		'label'=> '',
	        		'value'=> Yii::getAlias('@storageUrl/folie/spots/' . $model->avatar),
	        		'format' => ['image',['width'=>'600','height'=>'100']],
	        	],
	            'id',
	        	'name',
	            'city.name',
	            'description:ntext',
	            'address',
	        	[
	        		'attribute' => 'status',
	        		'format' => 'html',
	        		'value' => $model->status ? '<span class="glyphicon glyphicon-ok text-success"></span>' :
	        			'<span class="glyphicon glyphicon-remove text-danger"></span>',
	        	], 
	        ],
	    ]); ?>
    	<?php /*if ($model->sportsValues) : ?>
			<div class="article-meta">
				<span class="glyphicon glyphicon-tags"></span> <?= $model->sportLinks ?>
			</div>
		<?php endif */?>	
	</div>    
</div>

<p>
	<?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
	<?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
		'class' => 'btn btn-danger',
		'data' => [
			'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
			'method' => 'post',
		],
	]) ?>
</p>