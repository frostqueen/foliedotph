<?php

/* @var $this yii\web\View */
/* @var $model common\models\Char */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Char',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Chars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>

<div class="char-update layer1">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
