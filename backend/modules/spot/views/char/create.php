<?php

/* @var $this yii\web\View */
/* @var $model common\models\Char */

$this->title = Yii::t('backend', 'Add New Char');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Chars'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="char-create layer1">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
