<?php

use yii\widgets\ListView;

?>

<div class="category-list">
	<div class="category-header"><p><?php echo $model->name; ?></p></div>
	<?= ListView::widget([
		'dataProvider' => $subcatDataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => '_subcategory',
		'viewParams'=>['subid' => $model->id],
		'summary'=>'',
    ]) ?>
</div>
