<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\SportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Sports');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sports-grid col-md-12">
	<div class="box-body">

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
	
	<section class="panel panel-default sports-box box">
  		<div class="panel-heading">Sports</div>
		<div class="panel-body">
		
    <p>
        <?= Html::a(Yii::t('backend', 'Create Sports'), ['create'], ['class' => 'btn btn-success pull-right']) ?>
    </p>
		    <?= GridView::widget([
		        'dataProvider' => $dataProvider,
		        'filterModel' => $searchModel,
		        'columns' => [
		            ['class' => 'yii\grid\SerialColumn'],
		            'name',
		            //'avatar',
		            //'description:ntext',
		            'status',
		            'parent_id',
		            'frequency',
		
		            ['class' => 'yii\grid\ActionColumn'],
		        ],
		    ]); ?>    				         
		</div>
		<div class="panel-footer"><a href="">See More</a></div>
	</section>

    <?php Pjax::end(); ?>
</div>
</div>
