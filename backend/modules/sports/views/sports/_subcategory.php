<?php

use yii\helpers\Html;

if ($model->parent_id == $subid){ ?>
	<div class="col-md-4" style="margin-bottom:25px;">
		<?php if ($model->avatar) : ?>
			<?= Html::a(Html::img('@storageUrl/folie/sports/'.$model->avatar, ['class' => 'result-img']), ['view', 'id' => $model->id]) ?>
		<?php else: ?>
			<?= Html::a(Html::img('@storageUrl/folie/bg.jpg', ['class' => 'result-img']), ['view', 'id' => $model->id]) ?>
		<?php endif ?>
		<div class="thumb-link"><?= Html::a(Yii::t('backend', $model->name), ['view', 'id' => $model->id], []) ?></div>
	</div>
<?php } ?>