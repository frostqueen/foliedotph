<?php

/* @var $this yii\web\View */
/* @var $model common\models\Sports */

$this->title = Yii::t('backend', 'Add New Sports');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Sports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="sports-box-image col-md-8"">
	<section class="panel panel-default sports-box box">
		<div class="panel-body">
		    <?= $this->render('_form', [
		        'model' => $model,
		    	'sports' => $sports,
		    	'sports_status' => $sports_status,
		    ]) ?>
		</div>
	</section>
</div>

