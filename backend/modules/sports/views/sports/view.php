<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\helpers\ArrayHelper;
use yii\widgets\DetailView;
use common\models\Sports;
use yii\widgets\ListView;
use yii\grid\GridView;
use yii\helpers\Url;
use tpmanc\imagick\Imagick;
use Imagine\Imagick\Imagine;
/* @var $this yii\web\View */
/* @var $model common\models\Sports */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Sports'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="sports-box-image col-md-8">
	<div class="box-body">
		<p>
	        <?php if ($model->avatar) : ?>

	        <?php Imagick::open(Yii::getAlias('@storageUrl/folie/sports/' . $model->avatar))->thumb(200, 200); ?>
	        
	            <img src="<?= Yii::getAlias('@storageUrl/folie/sports/' . $model->avatar) ?>" class="sports-img">
	        <?php else: ?>
	            <img src="<?= Yii::getAlias('@storageUrl/folie/bg.jpg') ?>" class="sports-img">
	        <?php endif ?>
	    </p>
	    <section class="panel panel-default sports-box box">
  			<div class="panel-heading">Upcoming Events</div>
			<div class="panel-body">
					<?= ListView::widget([
				        'dataProvider' => $eventProvider,
				        'itemOptions' => ['class' => 'item'],
				        'itemView' => '_events',
				        'summary' => false,
    				]) ?>
    				         
			</div>
			<div class="panel-footer"><a href="">See More</a></div>
		</section>
		
		<section class="panel panel-default sports-box box">
  			<div class="panel-heading">Spots</div>
			<div class="panel-body">
				The quick brown fox jumps over the lazy dog near the bank of the river.
			</div>
			<div class="panel-footer"><a href="">See More</a></div>
		</section>
	</div>
</div>
<div class="col-md-4">
	<div class="panel panel-default">
	  <div class="panel-body">
	    <p>
	        <?= Html::a(Yii::t('backend', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
	        <?= Html::a(Yii::t('backend', 'Delete'), ['delete', 'id' => $model->id], [
	            'class' => 'btn btn-danger',
	            'data' => [
	                'confirm' => Yii::t('backend', 'Are you sure you want to delete this item?'),
	                'method' => 'post',
	            ],
	        ]); ?>
	        
	    </p>
	    <?= DetailView::widget([
	        'model' => $model,
	        'attributes' => [
	            //'id',
	            'name',
	            'avatar',
	            //'description:ntext',
	        	[
	        		'attribute' => 'parent_id',
	        		'value' => $model->parent ? $model->parent->name : null,        		
	        	],
	        	[
	        		'attribute' => 'status',
	        		'format' => 'html',
	        		'value' => $model->status ? '<span class="glyphicon glyphicon-ok text-success"></span>' : 
	        			'<span class="glyphicon glyphicon-remove text-danger"></span>',
	        	],
	        	'frequency'
	        ],
	    ]) ?>		
	  </div>
	</div>
	
	<div class="panel panel-default">
		<div class="panel-heading">Event Photos</div>
		<div class="panel-body">
	    	The quick brown fox jumps over the lazy dog near the bank of the river.
		</div>
		<div class="panel-footer"><a href="">View Albums</a></div>
	</div>
	
	<div class="panel panel-default">
		<div class="panel-heading">
			<?php 
				if ($model->parent_id == null){ 
					echo 'List of ' . $model->name .'';
				}else{
					echo 'Related Sports';
				}
			?>
		</div>
		<div class="panel-body">
		    <?= GridView::widget([
		        'dataProvider' => $dataProvider,
		        'showHeader' => false,
		        'summary' => '',
		        'columns' => [
		        	[
		        		'label'=> 'name',
		        		'format' => 'raw',
		        		'value'=>function ($data) {
		        			return Html::a($data->name,['sports/view', 'id' => $data->id]);
	        			},
		        	],
		        ],
		    ]) ?>
		</div>
		<div class="panel-footer">
			<?php 
				if ($model->parent_id == null){ 
					echo Html::a('View all '. $model->name . ' Sports',['sports/list', 'ListSearch[parent_id]' => $model->id]);
				}else{
					echo Html::a('View all '. $model->parent->name . ' Sports',['sports/list', 'ListSearch[parent_id]' => $model->parent_id]);
				}
			?>
		</div>
	</div>
</div>


