<?php

use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Sports');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="sports-box-image col-md-8">
	<div class="box-body">
		<section class="panel panel-default sports-box box">
		<div class="panel-heading">Sports</div>
			<div class="panel-body">
				<?= ListView::widget([
			        'dataProvider' => $catDataProvider,
			        'itemOptions' => ['class' => 'item'],
			        'itemView' => '_category',
					'viewParams'=>['subcatDataProvider' => $subcatDataProvider],
					'summary'=>'',
			    ]) ?>
		    </div>
		</section>
	</div>
</div>
<div class="col-md-4">
	<div class="panel panel-default">
		<div class="panel-heading">Search</div>
		<div class="panel-body">
	    	<?php echo $this->render('_search', ['model' => $listModel]); ?>
		</div>
	</div>
</div>
