<?php
use yii\helpers\Html;
?>
<div class="col-md-3 search-list">

	<?php if ($model->avatar) : ?>
		<?= Html::a(Html::img('@storageUrl/folie/events/'.$model->avatar, ['class' => 'result-img']), ['/events/events/view', 'id' => $model->id]) ?>
	<?php else: ?>
		<?= Html::a(Html::img('@storageUrl/folie/bg.jpg', ['class' => 'result-img']), ['/events/events/view', 'id' => $model->id]) ?>
	<?php endif ?>
	<?php echo Yii::$app->formatter->asDate($model->dateStart); ?>
	<?php echo Yii::t('backend', $model->name); ?>
    
</div>
