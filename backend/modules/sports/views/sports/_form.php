<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use vova07\fileapi\Widget;
use dosamigos\selectize\SelectizeTextInput;
use trntv\yii\datetime\DateTimeWidget;
//use vova07\imperavi\Widget as Description;

/* @var $this yii\web\View */
/* @var $model common\models\Sports */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sports-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'avatar')->widget(
        Widget::className(),
        [
            'settings' => [
                'url' => ['/site/fileapi-upload'],
            ],
        ]
    ) ?>

	<?php /*= $form->field($model, 'description')->widget(Description::className(), [
        'settings' => [
            'minHeight' => 200,
            'plugins' => [
            ],
            'imageManagerJson' => Url::to(['/site/images-get']),
            'imageUpload' => Url::to(['/site/image-upload']),
        ],
    ])*/ ?>

    <?= $form->field($model, 'status')->label(Yii::t('backend', 'Status'))->radioList($sports_status) ?>
	
	<?= $form->field($model, 'parent_id')->dropDownList($sports, ['prompt' => '']) ?>
	
	 
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('backend', 'Save') : Yii::t('backend', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
