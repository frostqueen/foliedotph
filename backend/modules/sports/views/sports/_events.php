<?php

use yii\helpers\Html;

?>

<div class="col-md-4" style="margin-bottom:30px;">
	<?php if ($model->avatar) : ?>
		<?= Html::a(Html::img('@storageUrl/folie/events/'.$model->avatar, ['class' => 'result-img']), ['/events/events/view', 'id' => $model->id]) ?>
	<?php else: ?>
		<?= Html::a(Html::img('@storageUrl/folie/bg.jpg', ['class' => 'result-img']), ['/events/events/view/', 'id' => $model->id]) ?>
	<?php endif ?>
	<div class="thumb-link">
		<p><?php echo Yii::t('backend', $model->name); ?></p>
		<p><?php echo Yii::$app->formatter->asDate($model->dateStart); ?></p>
    </div>
</div>
