<?php

namespace backend\modules\sports;

use yii\base\Module as BaseModule;

/**
 * sports module definition class
 */
class Module extends BaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\sports\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
