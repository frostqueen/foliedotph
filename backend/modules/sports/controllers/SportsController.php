<?php

namespace backend\modules\sports\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\helpers\ArrayHelper;
use yii\filters\VerbFilter;
use common\models\Sports;
use backend\modules\sports\models\search\CategorySearch;
use backend\modules\sports\models\search\SportsSearch;
use backend\modules\sports\models\search\ListSearch;
use backend\modules\sports\models\search\SportsGridSearch;
use backend\modules\events\models\search\EventsInSportsSearch;
use common\models\Events;
use yii\data\ActiveDataProvider;
use yii\web\Response;

/**
 * SportsController implements the CRUD actions for Sports model.
 */
class SportsController extends Controller
{
    /**
     * @inheritdoc
     */
	public $layout = '@app/views/layouts/sports-view';
	
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    
    /**
     * Lists all Sports models.
     * @return mixed
     */
    public function actionList()
    {
    	$searchModel = new ListSearch();
    	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    	return $this->render('list', [
    		'searchModel' => $searchModel,
    		'dataProvider' => $dataProvider,
    	]);
    }
    
    /**
     * Lists all Sports models in grid view.
     * @return mixed
     */
    public function actionGrid()
    {
    	$searchModel = new SportsGridSearch();
    	$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    
    	return $this->render('grid', [
    		'searchModel' => $searchModel,
    		'dataProvider' => $dataProvider,
    	]);
    }

    /**
     * @inheritdoc
     */
    public function actionItems()
    {
    	Yii::$app->response->format = Response::FORMAT_JSON;
    	$items = Sports::find()->select(['name'])->orderBy(['name' => SORT_ASC])->all();
    
    	return $items;
    }
    /**
     * Lists all Sports models.
     * @return mixed
     */
    public function actionIndex()
    {
    	$listModel = new ListSearch();
    	$listProvider = $listModel->search(Yii::$app->request->queryParams);
        $catQuery = Sports::find()
        			->where(['parent_id' =>null,'status'=>1]);
        $catDataProvider = new ActiveDataProvider([
	    					'query' => $catQuery,
	    					'pagination' => false,
	    					'sort'=> ['defaultOrder' => ['name'=>SORT_ASC]]]);
        
        $subcatQuery = Sports::find()
        				->where(['not',['parent_id' => null]])
	        			->where(['status' => 1]);
        $subcatDataProvider = new ActiveDataProvider([
	 	 						'query' => $subcatQuery,
	    						'pagination' => false,
	    						'sort'=> ['defaultOrder' => ['name'=>SORT_ASC]]]);
        $sports_status = Sports::statuses();
        return $this->render('index', [
            'catDataProvider' => $catDataProvider,
        	'subcatDataProvider' => $subcatDataProvider,
        	'listModel' => $listModel,
        	'listProvider' => $listProvider,
        	'sports_status' => $sports_status,
        ]);
    }

    /**
     * Displays a single Sports model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
    	$model = $this->findModel($id);
    	if ($model->parent_id != null){
	    	$query = Sports::find()->limit(5)
	    		->where(['parent_id' => $this->findModel($id)->parent_id])
	    		->andWhere(['<>','id', $id]);
    	}else{
    		$query = Sports::find()->limit(5)
    			->where(['parent_id' => $model->id])
    			->andWhere(['<>','id', $id]);
    	}
    	$dataProvider = new ActiveDataProvider([
    		'query' => $query,
    		'pagination' => false,
    		'sort'=> ['defaultOrder' => ['name'=>SORT_ASC]]
    	]);
    	
    	$event = Events::find()->joinWith('tags')->where('{{%sports}}.id = :id', [':id' => $id]);
    	
    	$eventProvider = new ActiveDataProvider([
    		'query' => $event,
    		
    	]);
    	
    	return $this->render('view', [
    		'model' => $model,
    		'eventProvider' => $eventProvider,
    		'dataProvider' => $dataProvider,
    	]);
   
    }

    /**
     * Creates a new Sports model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Sports();        
        $sports = Sports::find()->noParents()->all();
        $sports = ArrayHelper::map($sports, 'id', 'name');
        $sports_status = Sports::statuses();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            	'sports'=> $sports,
            	'sports_status' => $sports_status,
            ]);
        }
    }

    /**
     * Updates an existing Sports model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $sports = Sports::find()->noParents()->all();
        $sports = ArrayHelper::map($sports, 'id', 'name');
        $sports_status = Sports::statuses();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
        	Yii::$app->session->setFlash('success', Yii::t('backend', 'Sports details was successfully saved.'));
        	//return $this->refresh();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            	'sports' => $sports,
            	'sports_status' => $sports_status,
            ]);
        }
    }

    /**
     * Deletes an existing Sports model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->setFlash('success', Yii::t('backend', 'Sports was successfully deleted.'));
        return $this->redirect(['index']);
    }

    /**
     * Finds the Sports model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Sports the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Sports::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
