<?php

namespace backend\modules\place;

use yii\base\Module as BaseModule;

/**
 * place module definition class
 */
class Module extends BaseModule
{
    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'backend\modules\place\controllers';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
