<?php

/* @var $this yii\web\View */
/* @var $model common\models\Country */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'Country',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Countries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->name]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>

<div class="country-update layer1">

    <?= $this->render('_form', [
        'model' => $model,
    	'country_status' => $country_status,
    ]) ?>

</div>
