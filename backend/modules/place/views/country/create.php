<?php

/* @var $this yii\web\View */
/* @var $model common\models\Country */

$this->title = Yii::t('backend', 'Add New Country');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Countries'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="country-create layer1">

    <?= $this->render('_form', [
        'model' => $model,
    	'country_status' => $country_status,
    ]) ?>

</div>
