<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\Country;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CountrySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Countries');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="country-index">

    <?php  //echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Create Country'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
	<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            'name',
            'iso3',
        	[
        		'attribute' => 'status',
        		'format' => 'html',
        		'value' => function ($model) {
        		return $model->status ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>';
        		},
        		'filter' => [
        				Country::STATUS_INACTIVE => Yii::t('backend', 'Inactive'),
        				Country::STATUS_ACTIVE => Yii::t('backend', 'Active'),
        		],
        	],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
	<?php Pjax::end(); ?>
	
</div>
