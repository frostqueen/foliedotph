<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\ArrayHelper;
use common\models\City;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'Cities');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="city-index layer1">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Add New City'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
	<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            'name',
           'state.country_name',
        	[
        		'attribute' => 'state.id',
        		'value' => 'state.name',
        		'filter' => Html::activeDropDownList($searchModel, 'state_id', $state, ['prompt' => 'Select Province']),
        	],
        	[
        		'attribute' => 'status',
        		'format' => 'html',
        		'value' => function ($model) {return $model->status ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>';},
        		'filter' => [
        			City::STATUS_INACTIVE => Yii::t('backend', 'Inactive'),
        			City::STATUS_ACTIVE => Yii::t('backend', 'Active'),
        		],
        	],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	
	<?php Pjax::end(); ?>

</div>
