<?php

/* @var $this yii\web\View */
/* @var $model backend\models\City */

$this->title = Yii::t('backend', 'Add New City');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Cities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="city-create layer1">

    <?= $this->render('_form', [
        'model' => $model,
    	'state' => $state,
    	'city_status' => $city_status,
    ]) ?>

</div>
