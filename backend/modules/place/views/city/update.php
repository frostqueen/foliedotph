<?php

/* @var $this yii\web\View */
/* @var $model common\models\City */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
	'modelClass' => 'City',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'Cities'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>

<div class="city-update layer1">

    <?= $this->render('_form', [
        'model' => $model,
    	'state' => $state,
    	'city_status' => $city_status,
    ]) ?>

</div>
