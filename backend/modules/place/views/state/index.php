<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use common\models\State;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\StateSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('backend', 'States');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="state-index layer1">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('backend', 'Add New Province'), ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    
	<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],
            //'id',
            'name',
            //'country_name',
        	[
        		'attribute' => 'country_name',
        		'value' => 'country_name',
        		'filter' => Html::activeDropDownList($searchModel, 'country_name', $country, ['prompt' => 'Select Country']),
        	],
        	[
        		'attribute' => 'status',
        		'format' => 'html',
        		'value' => function ($model) {
        		return $model->status ? '<span class="glyphicon glyphicon-ok text-success"></span>' : '<span class="glyphicon glyphicon-remove text-danger"></span>';
        		},
        		'filter' => [
        				State::STATUS_INACTIVE => Yii::t('backend', 'Inactive'),
        				State::STATUS_ACTIVE => Yii::t('backend', 'Active'),
        		],
        	],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    
	<?php Pjax::end(); ?>

</div>
