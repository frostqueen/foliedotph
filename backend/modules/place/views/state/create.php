<?php

/* @var $this yii\web\View */
/* @var $model common\models\State */

$this->title = Yii::t('backend', 'Add New Province');
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'States'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="state-create layer1">

    <?= $this->render('_form', [
        'model' => $model,
    	'state_status' => $state_status,
    	'country' => $country,
    ]) ?>

</div>
