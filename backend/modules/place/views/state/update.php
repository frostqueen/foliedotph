<?php

/* @var $this yii\web\View */
/* @var $model backend\models\State */

$this->title = Yii::t('backend', 'Update {modelClass}: ', [
    'modelClass' => 'State',
]) . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('backend', 'States'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('backend', 'Update');
?>

<div class="state-update layer1">

    <?= $this->render('_form', [
        'model' => $model,
    	'state_status'=> $state_status,
        'country'=> $country,
    ]) ?>

</div>
