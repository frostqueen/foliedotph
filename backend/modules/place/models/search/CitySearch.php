<?php

namespace backend\modules\place\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\City;

/**
 * CitySearch represents the model behind the search form about `frontend\models\City`.
 */
class CitySearch extends City
{
	public $state;
	public $country_name;
	
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'state_id'], 'integer'],
            [['name', 'state_id', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = City::find();
        $query->joinWith(['state']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        	'sort'=> ['defaultOrder' => ['name'=>SORT_ASC]]
        ]);

        $dataProvider->sort->attributes['state_id'] = [
        		'asc' => ['state.name' => SORT_ASC],
        		'desc' => ['state.name' => SORT_DESC],
        ];
        
        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'city.name', $this->name])
        ->andFilterWhere(['like', 'city.status', $this->status])
        ->andFilterWhere(['like', 'state.id', $this->state_id])
        ->andFilterWhere(['like', 'state.country_name', $this->country_name])
       ;
        
        return $dataProvider;
    }
}
