<?php

return [
	'class' => 'codemix\localeurls\UrlManager',
	'languages' => ['en-US', 'en'],
    //'class' => 'yii\web\UrlManager',
    'enablePrettyUrl' => true,
    'showScriptName' => false,
    'rules' => [
		'spot/' => 'spot/spot/index',
    	'spot/create' => 'spot/spot/create',
    	'spot/<id:\d+>' => 'spot/spot/view',
    	'spot/update/<id:\d+>' => 'spot/spot/update',
    	
    	'sports/' => 'sports/sports/index',
    	'sports/create' => 'sports/sports/create',
    	'sports/<id:\d+>' => 'sports/sports/view',
    	'sports/update/<id:\d+>' => 'sports/sports/update',
    	'sports/items' => 'sports/sports/items',
    	'sports/grid' => 'sports/sports/grid',
    	
    	'events/' => 'events/events/index',
    	'events/create' => 'events/events/create',
    	'events/<id:\d+>' => 'events/events/view',
   		'events/update/<id:\d+>' => 'events/events/update',
        

    ],
];
