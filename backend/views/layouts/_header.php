<?php

use yii\bootstrap\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use backend\models\Log;

/* @var $this \yii\web\View */
?>
<header class="main-header">
	<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		<span class="sr-only">Toggle navigation</span>
	</a>
    <a href="<?= Yii::$app->homeUrl ?>" class="logo">
	<span class="logo-mini">APP</span>
	<span class="logo-lg">
		<?php //echo Yii::$app->name; ?>
		<img src="<?= Yii::getAlias('@storageUrl/folie/foliedotph80.png') ?>">
	</span>
	</a>
    <nav class="navbar navbar-static-top container" role="navigation">
    
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
				<li class="dropdown tasks-menu">
                    <a href="/site/index">
                        <i class="fa fa-home"></i>
                    </a>
                </li>
                <li class="dropdown tasks-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="fa fa-warning"></i>
                        <span class="label label-danger"><?= Log::find()->count() ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="header"><?= Yii::t('backend', 'You have {num} log items', ['num' => Log::find()->count()]) ?></li>
                        <li>
                            <ul class="menu">
                                <?php foreach (Log::find()->orderBy(['log_time' => SORT_DESC])->limit(5)->all() as $logEntry): ?>
                                    <li>
                                        <a href="<?= Yii::$app->urlManager->createUrl(['/log/view', 'id' => $logEntry->id]) ?>">
                                            <i class="fa fa-warning <?= $logEntry->level == \yii\log\Logger::LEVEL_ERROR ? 'text-red' : 'text-yellow' ?>"></i>
                                            <?= $logEntry->category ?>
                                        </a>
                                    </li>
                                <?php endforeach ?>
                            </ul>
                        </li>
                        <li class="footer">
                            <?= Html::a(Yii::t('backend', 'View all'), ['/log/index']) ?>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="<?= Yii::$app->homeUrl ?>/site/settings"><i class="fa fa-cogs"></i></a>
                </li>
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <?php if (Yii::$app->user->identity->userProfile->avatar_path) : ?>
                            <img src="<?= Yii::getAlias('@storageUrl/avatars/' . Yii::$app->user->identity->userProfile->avatar_path) ?>" class="user-image">
                        <?php else: ?>
                            <img src="<?= Yii::$app->homeUrl . '/static/img/default.png' ?>" class="user-image">
                        <?php endif ?>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <?php if (Yii::$app->user->identity->userProfile->avatar_path) : ?>
                                <img src="<?= Yii::getAlias('@storageUrl/avatars/' . Yii::$app->user->identity->userProfile->avatar_path) ?>" class="img-circle pull-left">
                            <?php else: ?>
                                <img src="<?= Yii::$app->homeUrl . '/static/img/default.png' ?>" class="img-circle pull-left">
                            <?php endif ?>
                            <p>
                                <strong><?= Yii::$app->user->identity->username ?></strong>
                                <small><?= Yii::t('backend', Yii::$app->user->identity->email ) ?></small>
                                <a style="color:#3c8dbc;">Google + Profile</a><a style="color:#3c8dbc;">Policy</a>
								<?= Html::a(
                                    Yii::t('backend', 'My Account'),
                                    ['/user/update', 'id' => Yii::$app->user->id],
                                    ['class' => 'btn btn-primary btn-primary-header']
                                ) ?>
                            </p>
                            
                        </li>
                        <li class="user-footer">
                            <div class="pull-left">
                                <?= Html::a(
                                    Yii::t('frontend', 'Frontend'),
                                    ['/account/default/view', 'id' => Yii::$app->user->id],
                                    ['class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                            <div class="pull-right">
                                <?= Html::a(
                                    Yii::t('backend', 'Logout'),
                                    ['/site/logout'],
                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
                                ) ?>
                            </div>
                        </li>
                    </ul>
                </li>                
            </ul>
        </div>
    </nav>
</header>
