<?php

use backend\models\Log;
use backend\widgets\Menu;

/* @var $this \yii\web\View */
?>
<aside class="main-sidebar">
    <section class="sidebar">
        <?= Menu::widget([
            'options' => ['class' => 'sidebar-menu'],
            'linkTemplate' => '<a href="{url}">{icon}<span>{label}</span>{right-icon}{badge}</a>',
            'submenuTemplate' => "\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
            'activateParents' => true,
            'items' => [
            	[
            		'label' => Yii::t('frontend', 'Home'),
            		'url' => ['/site/index'],
            		'icon' => '<i class="fa fa-home"></i>',
            	],
            	[
            		'label' => Yii::t('frontend', 'My Events'),
            		'url' => ['/'],
            		'icon' => '<i class="fa fa-calendar-check-o"></i>',
            	],
            	[
            		'label' => Yii::t('backend', 'Explore'),
            		'options' => ['class' => 'header'],
            	],
            	[
            		'label' => Yii::t('backend', 'Sports'),
            		'url' => ['/sports'],
            		'icon' => '<i class="fa fa-bicycle"></i>',
            	],
            	[
            		'label' => Yii::t('backend', 'Events'),
            		'url' => ['/events'],
            		'icon' => '<i class="fa fa-calendar"></i>',
            	],
            	[
            		'label' => Yii::t('backend', 'Spots'),
            		'url' => '/spot',
            		'icon' => '<i class="fa fa-map-marker"></i>',
            	],
            	[
            		'label' => Yii::t('backend', 'Attribute'),
            		'options' => ['class' => 'header'],
            	],
            	[
            		'label' => Yii::t('backend', 'Spot'),
            		'url' => ['/spot/char/index'],
            		'icon' => '<i class="fa fa-tag"></i>',
            	],
            	[
            		'label' => Yii::t('backend', 'Places'),
            		'url' => '#',
            		'icon' => '<i class="fa fa-flag"></i>',
            		'options' => ['class' => 'treeview'],
            		'items' => [
            			[
            				'label' => Yii::t('backend', 'Country'),
            				'url' => ['/place/country/index'],
            				//'icon' => '<i class="fa fa-tag"></i>',
            			],
            			[
            				'label' => Yii::t('backend', 'State'),
            				'url' => ['/place/state/index'],
            				//'icon' => '<i class="fa fa-tag"></i>',
            			],
            			[
            				'label' => Yii::t('backend', 'City'),
            				'url' => ['/place/city/index'],
            				//'icon' => '<i class="fa fa-tag"></i>',
            			],	
            		],
            	],
            	[
            		'label' => Yii::t('backend', 'Administer'),
            		'options' => ['class' => 'header'],
            	],
                [
                    'label' => Yii::t('backend', 'Menu'),
                    'url' => ['/menu/index'],
                    'icon' => '<i class="fa fa-sitemap"></i>',
                ],
                [
                    'label' => Yii::t('backend', 'Tags'),
                    'url' => ['/tag/index'],
                    'icon' => '<i class="fa fa-tags"></i>',
                ],
                [
                    'label' => Yii::t('backend', 'Content'),
                    'url' => '#',
                    'icon' => '<i class="fa fa-edit"></i>',
                    'options' => ['class' => 'treeview'],
                    'items' => [
                        [
                        	'label' => Yii::t('backend', 'Static Pages'), 
                        	'url' => ['/page/index']
                        ],
                        [
                        	'label' => Yii::t('backend', 'Articles'), 
                        	'url' => ['/article/index']		
                        ],
                        [
                        	'label' => Yii::t('backend', 'Article Categories'), 
                        	'url' => ['/article-category/index']
                        ],
                    ],
                ],
                [
                    'label' => Yii::t('backend', 'Users'),
                    'url' => ['/user/index'],
                    'icon' => '<i class="fa fa-user"></i>',
                    'visible' => Yii::$app->user->can('administrator'),
                ],
            	
                [
                    'label' => Yii::t('backend', 'Other'),
                    'url' => '#',
                    'icon' => '<i class="fa fa-th-list"></i>',
                    'options' => ['class' => 'treeview'],
                    'items' => [
                        [
                            'label' => 'Gii',
                            'url' => ['/gii'],
                            //'icon' => '<i class="fa fa-angle-double-right"></i>',
                            //'visible' => YII_ENV_DEV,
                        ],
                        [
                            'label' => 'Web Shell',
                            'url' => ['/webshell'],                            
                            'visible' => Yii::$app->user->can('administrator'),
                        ],
                        [
                        	'label' => Yii::t('backend', 'File Manager'), 
                        	'url' => ['/file-manager/index'],                     		
                        ],
                        [
                            'label' => Yii::t('backend', 'DB Manager'),
                            'url' => ['/db-manager/default/index'],
                            'visible' => Yii::$app->user->can('administrator'),
                        ],
                        [
                        	'label' => Yii::t('backend', 'Key Storage'), 
                        	'url' => ['/key-storage/index'],                  		
                        ],
                        [
                        	'label' => Yii::t('backend', 'Cache'), 
                        	'url' => ['/cache/index'],                       		
                        ],
                        [
                            'label' => Yii::t('backend', 'Logs'),
                            'url' => ['/log/index'],
                            'badge' => Log::find()->count(), 'badgeBgClass' => 'label-danger',
                        ],
                    ],
                ],
            ],
        ]) ?>
	<div class="footer">
		<p class="pull-left"><a>Terms</a><a>Privacy</a><a>Security</a><a>Advertising</a><br/><a>Contact Us</a></p>
		<p class="pull-left"><?php echo Yii::$app->name ?> &copy; <?= date('Y') ?></p>
	</div>
    </section>
</aside>
