<?php

use yii\bootstrap\Html;
use yii\helpers\Inflector;
use yii\widgets\Breadcrumbs;
use lo\modules\noty\Wrapper;

/* @var $this \yii\web\View */
/* @var $content string */
?>
<div class="content-wrapper">
	
    <section class="content">
        <?= Wrapper::widget() ?>
        <div class="box">
			<section class="content-header layer-b-header-white">
		        <?php if (isset($this->blocks['content-header'])) { ?>
		            <h1><?= $this->blocks['content-header'] ?></h1>
		            
		        <?php } else { ?>
		            <h1>
		                <?php
		                if ($this->title !== null) {
		                    //echo Html::encode($this->title); ?>
		                    <?= Breadcrumbs::widget([
		                    		'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		                    		'homeLink'=>false
		                    ]) ?><?php 
		                } else {
		                    //echo Inflector::camel2words(Inflector::id2camel($this->context->module->id));
		                    echo ($this->context->module->id !== Yii::$app->id) ? '<small>Module</small>' : '';
		                } ?>
		            </h1>
		        <?php } ?>
		
		        
		    </section>
			<div class="box-body layer-b-white">
                <?= $content ?>
            </div>
        </div>
    </section>
</div>
