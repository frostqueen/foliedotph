<?php

use yii\widgets\Breadcrumbs;
use lo\modules\noty\Wrapper;

/* @var $this \yii\web\View */
/* @var $content string */
?>
<div class="content-wrapper">
	
    <section class="content">
    	<?php //= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : []]) ?>
        <?= Wrapper::widget() ?>
		<?= $content ?>
    </section>
</div>
