<?php

use yii\bootstrap\Html;
use backend\assets\AppAsset;
use lo\modules\noty\Wrapper;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="login-page">
<?php echo Wrapper::widget([
	'layerClass' => 'lo\modules\noty\layers\Noty',
	'layerOptions'=>[
		'layerId' => 'noty-layer',
		'overrideSystemConfirm' => true,
		'showTitle' => false,
		'registerAnimateCss' => true,
		'registerButtonsCss' => false
	],
	'options' => [
		'dismissQueue' => true,
		'animation'=>[
			'open'=> 'animated fadeInDown',
			'close'=> 'animated fadeOut',
			'easing'=>'swing',
			'speed'=>'500',
		],
		'layout' => 'topCenter',
		'timeout' => 3000,
		'theme' => 'relax',
	],
]);?>
<?php $this->beginBody() ?>
    <?= $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
