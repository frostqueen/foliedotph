<?php

use yii\helpers\Html;

?>

<div class="col-md-4" style="margin-bottom:30px;">
	<?php if ($model->avatar) : ?>
		<?= Html::a(Html::img('@storageUrl/folie/spots/'.$model->avatar, ['class' => 'result-img']), ['spot/view', 'id' => $model->id]) ?>
	<?php else: ?>
		<?= Html::a(Html::img('@storageUrl/folie/bg.jpg', ['class' => 'result-img']), ['spot/view', 'id' => $model->id]) ?>
	<?php endif ?>
	<div class="thumb-link">
		<?= Html::a(Yii::t('backend', $model->name), ['spot/view', 'id' => $model->id], []) ?>
	</div>
</div>
