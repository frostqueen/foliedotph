<?php
use yii\helpers\Html;
?>
<div class="col-md-4">
	<?php if ($model->avatar) : ?>
		<?= Html::a(Html::img('@storageUrl/folie/events/'.$model->avatar, ['class' => 'result-img']), ['events/events/view', 'id' => $model->id]) ?>
	<?php else: ?>
		<?= Html::a(Html::img('@storageUrl/folie/bg.jpg', ['class' => 'result-img']), ['events/events/view', 'id' => $model->id]) ?>
	<?php endif ?>
	<div class="thumb-link">
		<p><?= Html::a(Yii::t('backend', $model->name), ['events/events/view', 'id' => $model->id], []) ?></p>
		<p><?php echo Yii::$app->formatter->format($model->dateStart, 'date') .' &#8729; '. $model->address; ?></p>
	</div>
</div>
