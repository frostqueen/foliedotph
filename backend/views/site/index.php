<?php
use yii\widgets\ListView;
/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */
$this->title = Yii::t('backend', 'Folie');
?>

<div class="sports-box-image col-md-8">
	<div class="box-body">
		<section class="panel panel-default sports-box box">
  			<div class="panel-heading">Latest Events</div>
			<div class="panel-body">					
				<?= ListView::widget([
					'dataProvider' => $eventsDataProvider,
					'itemOptions' => ['class' => 'item'],
					'itemView' => '_events',
					'summary'=>'',
				]) ?>
			</div>
			<div class="panel-footer"><a href="/events/events">Browse All Events</a></div>
		</section>
		<section class="panel panel-default sports-box box">
  			<div class="panel-heading">Top Spots</div>
			<div class="panel-body">
				<?= ListView::widget([
					'dataProvider' => $spotsDataProvider,
					'itemOptions' => ['class' => 'item'],
					'itemView' => '_spots',
					'summary'=>'',
				]) ?>
			</div>
			<div class="panel-footer"><a href="/spot/spot">Browse All Spots</a></div>
		</section>
	    <section class="panel panel-default sports-box box">
  			<div class="panel-heading">Sports Category</div>
			<div class="panel-body">					
				<?= ListView::widget([
					'dataProvider' => $sportsDataProvider,
					'itemOptions' => ['class' => 'item'],
					'itemView' => '_sports',
					'summary'=>'',
				]) ?>
			</div>
		</section>
	</div>
</div>
<div class="col-md-4">
	<div class="panel panel-default sports-box-sidebar">
		<div class="panel-heading">Event Photos</div>
		<div class="panel-body">
	    	The quick brown fox jumps over the lazy dog near the bank of the river.
		</div>
		<div class="panel-footer"><a href="">View Albums</a></div>
	</div>
</div>