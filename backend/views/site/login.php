<?php

use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model common\models\LoginForm */

$this->title = Yii::t('backend', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
$this->params['body-class'] = 'login-page';
?>
<div class="backend-login-box">
    <div class="login-box-body">
    	<h4>Administrator Login </h4><hr/>
        <?php $form = ActiveForm::begin(['id' => 'login-form']) ?>
	        <div class="body">
				<?= $form->field($model, 'identity',['inputOptions' => 
	            	['placeholder'=>'Enter your username']]
	            	)->label(false) ?>
	            <?= $form->field($model, 'password',['inputOptions' => 
	            	['placeholder'=>'Password']]
	            	)->label(false)->passwordInput() ?>
	        </div>
	        <div class="footer">
	            <?= Html::submitButton('Sign In', ['class' => 'btn btn-primary btn-flat btn-block']) ?>
	        </div>
        <?php ActiveForm::end() ?>
    </div>
</div>
