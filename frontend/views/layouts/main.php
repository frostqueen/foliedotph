<?php

use yii\helpers\Html;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use frontend\models\NavItem;
use lo\modules\noty\Wrapper;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
	<!DOCTYPE html>
	<html lang="<?= Yii::$app->language ?>">
	<head>
	    <meta charset="<?= Yii::$app->charset ?>">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <?= Html::csrfMetaTags() ?>
	    <link rel="shortcut icon" sizes="16x16" href="<?= Yii::getAlias('@storageUrl/folie/favicon.ico') ?>" type="image/x-icon"/>
	    <title><?= Yii::$app->name . ' | ' . Html::encode($this->title) ?></title>
	    <?php $this->head() ?>
	</head>
	    <?= Html::beginTag('body', [
	        'class' => implode(' ', array_filter([
	            'hold-transition',
	            Yii::$app->keyStorage->get('backend.theme-skin', 'skin-blue'),
	            Yii::$app->keyStorage->get('backend.layout-fixed') ? 'fixed' : null,
	            Yii::$app->keyStorage->get('backend.layout-boxed') ? 'layout-boxed' : null,
	            Yii::$app->keyStorage->get('backend.layout-collapsed-sidebar') ? 'sidebar-collapse' : null,
	            Yii::$app->keyStorage->get('backend.layout-mini-sidebar') ? 'sidebar-mini' : null,
	        ])),
	    ]) ?>
	<body>
		<?php $this->beginBody() ?>
			<div class="wrap">
			    <div>
					<?= $this->render('_header.php') ?>
			        <?= $this->render('_left.php') ?>
			        <?= $this->render(
			            '_content.php',
			            ['content' => $content]) ?>  
			    </div>
			</div>
		<?php $this->endBody() ?>
	</body>
	</html>
<?php $this->endPage() ?>
