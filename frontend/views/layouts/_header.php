<?php

use yii\bootstrap\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use backend\models\Log;
use frontend\models\NavItem;

/* @var $this \yii\web\View */
?>

<header class="main-header">
	<a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
		<span class="sr-only">Toggle navigation</span>
	</a>
	<a href="<?= Yii::$app->homeUrl ?>" class="logo">
	<span class="logo-mini">APP</span>
	<span class="logo-lg">
		<?php //echo Yii::$app->name; ?>
		<img src="<?= Yii::getAlias('@storageUrl/folie/foliedotph80.png') ?>">
	</span>
	</a>
    <nav class="navbar navbar-static-top container" role="navigation">
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav navbar-custom-sub">
            	<a href="<?= Yii::$app->homeUrl . '/site/index'?>" class="btn btn-default event-button-layout">Create Event</a>
            	<?php if (!Yii::$app->user->isGuest) : ?>
	            	<li class="dropdown tasks-menu">
	                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	                        <i class="fa fa-envelope"></i>
	                        <span class="label label-danger"><?= Log::find()->count() ?></span>
	                    </a>
	                    <ul class="dropdown-menu">
	                        <li class="header"><?= Yii::t('backend', 'You have {num} log items', ['num' => Log::find()->count()]) ?></li>
	                        <li>
	                            <ul class="menu">
	                                <?php foreach (Log::find()->orderBy(['log_time' => SORT_DESC])->limit(5)->all() as $logEntry): ?>
	                                    <li>
	                                        <a href="<?= Yii::$app->urlManager->createUrl(['/log/view', 'id' => $logEntry->id]) ?>">
	                                            <i class="fa fa-warning <?= $logEntry->level == \yii\log\Logger::LEVEL_ERROR ? 'text-red' : 'text-yellow' ?>"></i>
	                                            <?= $logEntry->category ?>
	                                        </a>
	                                    </li>
	                                <?php endforeach ?>
	                            </ul>
	                        </li>
	                        <li class="footer">
	                            <?= Html::a(Yii::t('backend', 'View all'), ['/log/index']) ?>
	                        </li>
	                    </ul>
	                </li>
	                <li class="dropdown tasks-menu">
	                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	                        <i class="fa fa-bell"></i>
	                        <span class="label label-danger"><?= Log::find()->count() ?></span>
	                    </a>
	                    <ul class="dropdown-menu">
	                        <li class="header"><?= Yii::t('backend', 'You have {num} log items', ['num' => Log::find()->count()]) ?></li>
	                        <li>
	                            <ul class="menu">
	                                <?php foreach (Log::find()->orderBy(['log_time' => SORT_DESC])->limit(5)->all() as $logEntry): ?>
	                                    <li>
	                                        <a href="<?= Yii::$app->urlManager->createUrl(['/log/view', 'id' => $logEntry->id]) ?>">
	                                            <i class="fa fa-warning <?= $logEntry->level == \yii\log\Logger::LEVEL_ERROR ? 'text-red' : 'text-yellow' ?>"></i>
	                                            <?= $logEntry->category ?>
	                                        </a>
	                                    </li>
	                                <?php endforeach ?>
	                            </ul>
	                        </li>
	                        <li class="footer">
	                            <?= Html::a(Yii::t('backend', 'View all'), ['/log/index']) ?>
	                        </li>
	                    </ul>
	                </li>
					<li class="dropdown tasks-menu">
	                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	                        <i class="fa fa-bus"></i>
	                        <span class="label label-danger"><?= Log::find()->count() ?></span>
	                    </a>
	                    <ul class="dropdown-menu">
	                        <li class="header"><?= Yii::t('backend', 'You have {num} log items', ['num' => Log::find()->count()]) ?></li>
	                        <li>
	                            <ul class="menu">
	                                <?php foreach (Log::find()->orderBy(['log_time' => SORT_DESC])->limit(5)->all() as $logEntry): ?>
	                                    <li>
	                                        <a href="<?= Yii::$app->urlManager->createUrl(['/log/view', 'id' => $logEntry->id]) ?>">
	                                            <i class="fa fa-warning <?= $logEntry->level == \yii\log\Logger::LEVEL_ERROR ? 'text-red' : 'text-yellow' ?>"></i>
	                                            <?= $logEntry->category ?>
	                                        </a>
	                                    </li>
	                                <?php endforeach ?>
	                            </ul>
	                        </li>
	                        <li class="footer">
	                            <?= Html::a(Yii::t('backend', 'View all'), ['/log/index']) ?>
	                        </li>
	                    </ul>
	                </li>
                <?php else: ?>
                <a href="<?= Yii::$app->homeUrl . '/account/sign-in/login'?>" class="btn btn-primary event-button-layout">Sign In</a>
                <?php endif ?>
                <li class="dropdown user user-menu">   
					<?php if (!Yii::$app->user->isGuest) : ?>
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<img src="<?= Yii::getAlias('@storageUrl/avatars/' . Yii::$app->user->identity->userProfile->avatar_path) ?>" class="user-image" alt>
							<?php //echo Yii::$app->user->identity->username; ?>
						</a>
					<?php else: ?>
						<!-- li><a href="/account/sign-in/login">Log In</a></li>
						<li><a href="/account/sign-in/signup">Sign Up</a></li-->
					<?php endif ?>
                    <?php if (!Yii::$app->user->isGuest) : ?>
	                    <ul class="dropdown-menu">
	                        <li class="user-header">
	                            <?php if (Yii::$app->user->identity->userProfile->avatar_path) : ?>
	                                <img src="<?= Yii::getAlias('@storageUrl/avatars/' . Yii::$app->user->identity->userProfile->avatar_path) ?>" class="img-circle pull-left" alt>
	                            <?php else: ?>
	                                <img src="<?= Yii::$app->homeUrl . '/static/img/default.png' ?>" class="img-circle pull-left" alt>
	                            <?php endif ?>
	                            <p>
	                                <strong><?= Yii::$app->user->identity->username ?></strong>
	                                <small><?= Yii::t('backend', Yii::$app->user->identity->email ) ?></small>
	                                <a>Google + Profile</a><a>Policy</a>
	                           		<?= Html::a(
	                                    Yii::t('backend', 'Link Account'),
	                                    ['/account/default/view', 'id' => Yii::$app->user->id],
	                                    ['class' => 'btn btn-danger btn-primary-header']
	                                ) ?>
	                            </p>
	                        </li>
	                        <li class="user-footer">
	                            <div class="pull-left">
	                                <?= Html::a(
	                                    Yii::t('frontend', 'My Profile'),
	                                    ['/account/default/view', 'id' => Yii::$app->user->id],
	                                    ['class' => 'btn btn-primary']
	                                ) ?>
	                            </div>
	                            <div class="pull-right">
	                                <?= Html::a(
	                                    Yii::t('frontend', 'Logout'),
	                                    ['/account/sign-in/logout'],
	                                    ['data-method' => 'post', 'class' => 'btn btn-default btn-flat']
	                                ) ?>
	                            </div>
	                        </li>
	                    </ul>
                    <?php endif ?>
            </ul>
        </div>
    </nav>
    <!-- div id="w1-collapse" class="menu collapse navbar-collapse">
		<ul id="w2" class="navbar-nav  nav navbar-custom-sub">
			<li><a href="<?= Yii::$app->homeUrl?>">Home</a></li>
			<li><a href="<?= Yii::$app->homeUrl . '/promos/index' ?>">Promos</a></li>
			<li><a href="<?= Yii::$app->homeUrl . '/partnerships/index' ?>">Partnerships</a></li>
			<li><a href="<?= Yii::$app->homeUrl . '/article' ?>">Blog</a></li>
			<li><a href="<?= Yii::$app->homeUrl . '/site/contact' ?>">Talk to Us</a></li>
			<li class="dropdown"><a class="dropdown-toggle" href="#" data-toggle="dropdown">Talk to Us</a>
				<ul id="w3" class="dropdown-menu">
				<li><a href="">asdf</a></li>
				</ul>
			</li>
		</ul>
	</div-->
</header>