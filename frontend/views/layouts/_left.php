<?php

use frontend\widgets\Menu;

/* @var $this \yii\web\View */
?>

<aside class="main-sidebar">
    <section class="sidebar">
    	<?php if (!Yii::$app->user->isGuest) : ?>
			<div class="user-panel">
		        <div class="pull-left image">
		          <img src="<?= Yii::getAlias('@storageUrl/avatars/' . Yii::$app->user->identity->userProfile->avatar_path) ?>" class="user-image">
		        </div>
		        <div class="pull-left info">
		          <p><?php echo Yii::$app->user->identity->username; ?></p>
		          <a href="#"><i class="fa fa-pencil"></i> Edit Profile</a>
		        </div>
			</div>
		<?php else: ?>
        <?php endif ?>
        <?= Menu::widget([
            'options' => ['class' => 'sidebar-menu'],
            'linkTemplate' => '<a href="{url}">{icon}<span>{label}</span>{right-icon}{badge}</a>',
            'submenuTemplate' => "\n<ul class=\"treeview-menu\">\n{items}\n</ul>\n",
            'activateParents' => true,
            'items' => [
            	[
            		'label' => Yii::t('frontend', 'Home'),
            		'url' => ['/site/index'],
            		'icon' => '<i class="fa fa-home"></i>',
            	],
            	[
            		'label' => Yii::t('frontend', 'My Events'),
            		'url' => ['/'],
            		'icon' => '<i class="fa fa-calendar-check-o"></i>',
            	],
            	[
            		'label' => Yii::t('backend', 'Explore'),
            		'options' => ['class' => 'header'],
            	],
            	[
            		'label' => Yii::t('frontend', 'Sports'),
            		'url' => ['/'],
            		'icon' => '<i class="fa fa-bicycle"></i>',
            	],
            	[
            		'label' => Yii::t('frontend', 'Events'),
            		'url' => ['/'],
            		'icon' => '<i class="fa fa-calendar"></i>',
            	],
            	[
            		'label' => Yii::t('backend', 'Spots'),
            		'url' => '#',
            		'icon' => '<i class="fa fa-map-marker"></i>',
            	],
            	[
            		'label' => Yii::t('backend', 'Announcements'),
            		'options' => ['class' => 'header'],
            	],
            	[
            		'label' => Yii::t('frontend', 'Promos'),
            		'url' => ['/page/view', 'slug' => 'promos'],
            		'icon' => '<i class="fa fa-tag"></i>',
            	],
            	[
            		'label' => Yii::t('frontend', 'Articles'),
            		'url' => ['/article/index'],
            		'icon' => '<i class="fa fa-edit"></i>',
            	],
            	[
            		'label' => Yii::t('frontend', 'Talk to Us'),
            		'url' => ['/site/contact'],
            		'icon' => '<i class="fa fa-phone"></i>',
            	],
            	[
            		'label' => Yii::t('backend', 'Information Center'),
            		'options' => ['class' => 'header'],
            	],
            	[
            		'label' => Yii::t('frontend', 'Payment Center'),
            		'url' => ['/page/view', 'slug' => 'payment-centers'],
            		'icon' => '<i class="fa fa-credit-card"></i>', 
            	],
            	[
            		'label' => Yii::t('frontend', 'Safety Guidelines'),
            		'url' => ['/page/view', 'slug' => 'safety-guidelines'],
            		'icon' => '<i class="fa fa-support"></i>',
            	],
                [
                    'label' => Yii::t('frontend', 'Terms of Service'),
                    'url' => '#',
                    'icon' => '<i class="fa fa-legal"></i>',
                    'options' => ['class' => 'treeview'],
                    'items' => [
                        [
                        	'label' => Yii::t('frontend', 'Pre-paid Baggage'), 
                        	'url' => ['/page/view', 'slug' => 'prepaid-baggage'], 
                        ],
                        [
                        	'label' => Yii::t('frontend', 'Sports Baggage'), 
                        	'url' => ['/page/view', 'slug' => 'sports-baggage'],  
                        ],
                        [
                        	'label' => Yii::t('frontend', 'Online Seat Selector'), 
                        	'url' => ['/page/view', 'slug' => 'online-seat-selector'],  		
                        ],
                    	[
                    		'label' => Yii::t('frontend', 'Online Booking Cut-off'), 
                    		'url' => ['/page/view', 'slug' => 'online-booking-cutoff'], 
                    	],
                    ],
                ],
            	[
            		'label' => Yii::t('frontend', 'Help Center'),
            		'url' => ['/page/safety'],
            		'icon' => '<i class="fa fa-info"></i>',
            	],
            ],
        ]) ?>
	<div class="footer">
		<p class="pull-left"><a>Terms</a><a>Privacy</a><a>Security</a><a>Advertising</a><br/><a>Contact Us</a></p>
		<p class="pull-left"><?php echo Yii::$app->name ?> &copy; <?= date('Y') ?></p>
	</div>
    </section>
    
</aside>
