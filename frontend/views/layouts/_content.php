<?php

use yii\helpers\Inflector;
use yii\widgets\Breadcrumbs;
use lo\modules\noty\Wrapper;

/* @var $this \yii\web\View */
/* @var $content string */
?>

<div class="content-wrapper">
    <section class="content">
        <?= Wrapper::widget() ?>
        <div class="box">
			<div class="box-body">
				<!--<?= Breadcrumbs::widget([
					'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
				])  ?>
				<?= Wrapper::widget() ?>-->				
                <?= $content ?>
            </div>
        </div>
    </section>
    <?= $this->render('_footer.php') ?>
</div>
