<?php

use yii\helpers\Html;
use yii\helpers\HtmlPurifier;

//use dosamigos\disqus\Comments;
//use kartik\social\Disqus;
use kartik\social\FacebookPlugin;

/* @var $this yii\web\View */
/* @var $model common\models\Article */

Highlight::register($this);

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('frontend', 'Articles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-view">
    <article class="article-item">
		<h1 class="article-header">  
    		<?= Yii::t('frontend', '{title}', ['title' => $model->title]) ?>
    	</h1>
		<div class="layer-white">
	        <div class="article-meta">
	            <span class="glyphicon glyphicon-time"></span> <?= Yii::$app->formatter->asDatetime($model->published_at) ?>
	            <span class="glyphicon glyphicon-folder-close"></span> <?= Html::a($model->category->title, ['article/category', 'slug' => $model->category->slug]) ?>
	            <span class="glyphicon glyphicon-user"></span> <?= Html::a($model->author->username, ['account/default/view', 'id' => $model->author->id]) ?>
	            <?php if ($model->tagValues) : ?>
                    <div class="article-meta">
                        <span class="glyphicon glyphicon-tags"></span> <?= $model->tagLinks ?>
                    </div>
                <?php endif ?>
	        </div>	
	        <div class="row">
	            <div class="col-md-9">
	                <div class="article-text">
	                    <?= HtmlPurifier::process($model->body) ?>
	                </div>
                <hr/>
				<!--noindex-->
                    <?php /*= Comments::widget([
                        'shortname' => getenv('SHORT_NAME'),
                        'identifier' => Html::encode($model->slug),
                    ])*/ ?>
                <!--/noindex-->
					<?php echo $model->slug;?>
					<?php echo FacebookPlugin::widget(['type'=>FacebookPlugin::COMMENT, 'settings' => ['data-width'=>750, 'data-numposts'=>10, 'data-colorscheme'=>'dark', 'href'=>$model->slug, 'data-order-by'=>'social',]]);?>
	            </div>	
	            <!-- div class="col-md-3">
	                <?= $this->render(
	                    '_categoryItem.php',
	                    ['menuItems' => $menuItems]
	                ) ?>
	            </div-->
			</div>
        </div>
    </article>
</div>
