<?php

use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('frontend', 'Articles');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="article-index">
    <h1 class="article-header">  
    	<img src="<?= Yii::getAlias('@storageUrl/folie/foliedotph2.png') ?>" class="article-header-logo"> 
    	<?= Yii::t('frontend', 'Folie Blog') ?>
    </h1>
    <div class="row layer-white">
        <div class="col-md-9">
            <?= ListView::widget([
                'dataProvider' => $dataProvider,
                'itemView' => '_item',
                'summary' => false,
            ]) ?>
        </div>
        <div class="col-md-3">
            <?= $this->render(
                '_categoryItem.php',
                ['menuItems' => $menuItems]
            ) ?>
        </div>
    </div>
</div>
