<?php

use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model frontend\modules\account\models\SignupForm */

$this->title = Yii::t('frontend', 'Signup');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="register-box">
    <div class="register-box-body">
		<h4>Create your Folie Account </h4><hr/>
    	<?php $form = ActiveForm::begin() ?>
			<?= $form->field($model, 'username',['inputOptions' => 
	            ['placeholder'=>'Username']]
	            )->label(false)->textInput(['maxlength' => true]) ?>
	        <?= $form->field($model, 'email',['inputOptions' => 
				['placeholder'=>'Email']]
				)->label(false)->textInput(['maxlength' => true]) ?>
	        <?= $form->field($model, 'password',['inputOptions' => 
				['placeholder'=>'Password']]
				)->label(false)->passwordInput(['maxlength' => true]) ?>
	        <?= $form->field($model, 'password_confirm',['inputOptions' => 
				['placeholder'=>'Confirm your password']]
				)->label(false)->passwordInput(['maxlength' => true]) ?>
	        <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
				'captchaAction' => '/site/captcha',
				'template' => '<div class="row" style="margin-left:0;"><div class="col-lg-5">{input}</div><div class="col-lg-6">{image}</div></div>']) ?>
        <div class="form-group">
            <?= Html::submitButton(Yii::t('frontend', 'Signup'), ['class' => 'btn btn-primary btn-block']) ?>
        </div>
	</div>
    <?php ActiveForm::end() ?>
</div>
