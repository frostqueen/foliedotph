<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model frontend\modules\account\models\ResetPasswordForm */

$this->title = Yii::t('frontend', 'Reset password');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="request-password-box">
    <div class="request-password-body">
        <h4>You're almost there.</h4>
		<h5>Enter new password for your account</h5>
		<hr/>    	
    	<?php $form = ActiveForm::begin() ?>
    		<?= $form->field($model, 'password',['inputOptions' => 
				['placeholder'=>'Password']]
				)->label(false)->passwordInput(['maxlength' => true]) ?>
	        <?= $form->field($model, 'password_confirm',['inputOptions' => 
				['placeholder'=>'Confirm your password']]
				)->label(false)->passwordInput(['maxlength' => true]) ?>
	        <div class="form-group">
	            <?= Html::submitButton(Yii::t('frontend', 'Save'), ['class' => 'btn btn-primary btn-block']) ?>
	        </div>
	    <?php ActiveForm::end() ?>
	</div>
</div>
