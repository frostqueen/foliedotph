

            <div id="rev_slider_206_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="creative-freedom" style="background-color:#1f1d24;padding:0px;">
            
                <div id="rev_slider_206_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.1.1RC">
                
                    <ul>
                        <!-- SLIDE  -->
                        <li data-index="rs-688" data-transition="fadethroughdark" data-slotamount="default" 
                        data-easein="default" data-easeout="default" data-masterspeed="2000" 
                        data-thumb="@storageUrl/rs/Sail-Away-100x50.jpg" data-rotate="0" data-saveperformance="off" 
                        data-title="Creative" data-param1="01" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="@storageUrl/rs/Sail-Away.jpg" alt="" data-bgposition="center center" data-bgfit="cover" 
                            data-bgrepeat="no-repeat" data-bgparallax="3" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- BACKGROUND VIDEO LAYER -->
                            <div class="rs-background-video-layer" data-forcerewind="on" data-volume="mute" 
                            data-videowidth="100%" data-videoheight="100%" data-videomp4="<?= Yii::getAlias('@storageUrl/rs/41.mp4');?>" 
                            data-videopreload="preload" data-videoloop="loop" data-autoplay="true" 
                            data-autoplayonlyfirsttime="false" data-nextslideatend="true"></div>
                           
                        </li>
                        
                        
                    </ul>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
            </div>