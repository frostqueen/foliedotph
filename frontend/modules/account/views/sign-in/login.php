<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $login common\models\LoginForm */

$this->title = Yii::t('backend', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
$this->params['body-class'] = 'login-page';
?>

       
<div class="login-box">
    <div class="login-box-body">
        <?php $form = ActiveForm::begin(['validateOnBlur' => false,'id' => 'login-form']) ?>
        <div class="body">       
            <?= $form->field($login, 'identity',['inputOptions' => 
            	['placeholder'=>'Enter your username']]
            	)->label(false) ?>
            <?= $form->field($login, 'password',['inputOptions' => 
            	['placeholder'=>'Password']]
            	)->label(false)->passwordInput() ?>
        </div>

        <div class="footer">
            <?= Html::submitButton('Log In', ['class' => 'btn btn-primary btn-block']) ?>
            <a href="<?= Url::to(['sign-in/request-password-reset']) ?>" class="pull-right"><?= Yii::t('frontend', 'Forgot your password?') ?></a><br/>
        </div>
		
        <?php ActiveForm::end() ?>
    </div>
</div>

<div class="clearfix"></div>

<div class="login-box">
    <div class="login-box-body">
		<h4>Create your Folie Account </h4><hr/>
	
	    <?php $form = ActiveForm::begin() ?>
	        <?= $form->field($model, 'username',['inputOptions' => 
	            ['placeholder'=>'Username']]
	            )->label(false)->textInput(['maxlength' => true]) ?>
	        <?= $form->field($model, 'email',['inputOptions' => 
				['placeholder'=>'Email']]
				)->label(false)->textInput(['maxlength' => true]) ?>
	        <?= $form->field($model, 'password',['inputOptions' => 
				['placeholder'=>'Password']]
				)->label(false)->passwordInput(['maxlength' => true]) ?>
	        <?= $form->field($model, 'password_confirm',['inputOptions' => 
				['placeholder'=>'Confirm your password']]
				)->label(false)->passwordInput(['maxlength' => true]) ?>
	        <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
				'captchaAction' => '/site/captcha',
				'template' => '<div class="row" style="margin-left:0;"><div class="col-lg-5">{input}</div><div class="col-lg-6">{image}</div></div>']) ?>
	        <?= Html::submitButton(Yii::t('frontend', 'Signup'), ['class' => 'btn btn-primary btn-block']) ?>
	    <?php ActiveForm::end() ?>
	    
By signing up, you agree to the <a>Terms of Service</a> and <a>Privacy Policy</a>, including <a>Cookie Use</a>.
	    
	</div>
</div>   

<div id="rev_slider_206_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="creative-freedom" style="background-color:#1f1d24;padding:0px;">
            
                <div id="rev_slider_206_1" class="rev_slider fullscreenbanner" style="display:none;" data-version="5.1.1RC">
                
                    <ul>
                        <!-- SLIDE  -->
                        <li data-index="rs-688" data-transition="fadethroughdark" data-slotamount="default" 
                        data-easein="default" data-easeout="default" data-masterspeed="2000" 
                        data-thumb="@storageUrl/rs/Sail-Away-100x50.jpg" data-rotate="0" data-saveperformance="off" 
                        data-title="Creative" data-param1="01" data-description="">
                            <!-- MAIN IMAGE -->
                            <img src="@storageUrl/rs/Sail-Away.jpg" alt="" data-bgposition="center center" data-bgfit="cover" 
                            data-bgrepeat="no-repeat" data-bgparallax="3" class="rev-slidebg" data-no-retina>
                            <!-- LAYERS -->

                            <!-- BACKGROUND VIDEO LAYER -->
                            <div class="rs-background-video-layer" data-forcerewind="on" data-volume="mute" 
                            data-videowidth="100%" data-videoheight="100%" data-videomp4="<?= Yii::getAlias('@storageUrl/rs/41.mp4');?>" 
                            data-videopreload="preload" data-videoloop="loop" data-autoplay="true" 
                            data-autoplayonlyfirsttime="false" data-nextslideatend="true"></div>
                           
                        </li>
                        
                        
                    </ul>
                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                </div>
            </div>
            