<?php

use yii\captcha\Captcha;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model frontend\modules\account\models\PasswordResetRequestForm */

$this->title = Yii::t('frontend', 'Request password reset');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="request-password-box">
    <div class="request-password-body">
    	<h4>Having trouble signing in?</h4>
		<h5>Enter email associated with your account</h5>
		<hr/>
    	<?php $form = ActiveForm::begin() ?>
	        <?= $form->field($model, 'email',['inputOptions' => 
		            ['placeholder'=>'Enter Email']]
		            )->label(false)->textInput() ?>
	        <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
	            'captchaAction' => '/site/captcha',
	            'template' => '<div class="row" style="margin-left:0;"><div class="col-lg-5">{input}</div><div class="col-lg-6">{image}</div></div>',
	        ]) ?>
	        <div class="form-group">
	            <?= Html::submitButton(Yii::t('frontend', 'Request Recovery Email'), ['class' => 'btn btn-primary btn-block']) ?>
	            <a href="<?= Url::to(['sign-in/login']) ?>" class="pull-right"><?= Yii::t('frontend', '<< Go to back to Login') ?></a><br/>
	        </div>
    	<?php ActiveForm::end() ?>
    </div>    
</div>
