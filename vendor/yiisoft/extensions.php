<?php

$vendorDir = dirname(__DIR__);

return array (
  'vova07/yii2-imperavi-widget' => 
  array (
    'name' => 'vova07/yii2-imperavi-widget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@vova07/imperavi' => $vendorDir . '/vova07/yii2-imperavi-widget/src',
    ),
  ),
  'vova07/yii2-fileapi-widget' => 
  array (
    'name' => 'vova07/yii2-fileapi-widget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@vova07/fileapi' => $vendorDir . '/vova07/yii2-fileapi-widget',
    ),
  ),
  'creocoder/yii2-taggable' => 
  array (
    'name' => 'creocoder/yii2-taggable',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@creocoder/taggable' => $vendorDir . '/creocoder/yii2-taggable/src',
    ),
  ),
  '2amigos/yii2-disqus-widget' => 
  array (
    'name' => '2amigos/yii2-disqus-widget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@dosamigos/disqus' => $vendorDir . '/2amigos/yii2-disqus-widget/src',
    ),
  ),
  'mihaildev/yii2-elfinder' => 
  array (
    'name' => 'mihaildev/yii2-elfinder',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@mihaildev/elfinder' => $vendorDir . '/mihaildev/yii2-elfinder',
    ),
  ),
  'samdark/yii2-webshell' => 
  array (
    'name' => 'samdark/yii2-webshell',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@samdark/webshell' => $vendorDir . '/samdark/yii2-webshell',
    ),
  ),
  '2amigos/yii2-date-time-picker-widget' => 
  array (
    'name' => '2amigos/yii2-date-time-picker-widget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@dosamigos/datetimepicker' => $vendorDir . '/2amigos/yii2-date-time-picker-widget/src',
    ),
  ),
  'yiisoft/yii2-swiftmailer' => 
  array (
    'name' => 'yiisoft/yii2-swiftmailer',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/swiftmailer' => $vendorDir . '/yiisoft/yii2-swiftmailer',
    ),
  ),
  'beaten-sect0r/yii2-db-manager' => 
  array (
    'name' => 'beaten-sect0r/yii2-db-manager',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@bs/dbManager' => $vendorDir . '/beaten-sect0r/yii2-db-manager',
    ),
    'bootstrap' => 'bs\\dbManager\\Bootstrap',
  ),
  '2amigos/yii2-selectize-widget' => 
  array (
    'name' => '2amigos/yii2-selectize-widget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@dosamigos/selectize' => $vendorDir . '/2amigos/yii2-selectize-widget/src',
    ),
  ),
  'yiisoft/yii2-jui' => 
  array (
    'name' => 'yiisoft/yii2-jui',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/jui' => $vendorDir . '/yiisoft/yii2-jui',
    ),
  ),
  'trntv/yii2-datetime-widget' => 
  array (
    'name' => 'trntv/yii2-datetime-widget',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@trntv/yii/datetime' => $vendorDir . '/trntv/yii2-datetime-widget/src',
    ),
  ),
  'loveorigami/yii2-notification-wrapper' => 
  array (
    'name' => 'loveorigami/yii2-notification-wrapper',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@lo/modules/noty' => $vendorDir . '/loveorigami/yii2-notification-wrapper/src',
    ),
    'bootstrap' => 'lo\\modules\\noty\\Bootstrap',
  ),
  'kartik-v/yii2-krajee-base' => 
  array (
    'name' => 'kartik-v/yii2-krajee-base',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/base' => $vendorDir . '/kartik-v/yii2-krajee-base',
    ),
  ),
  'kartik-v/yii2-widget-datetimepicker' => 
  array (
    'name' => 'kartik-v/yii2-widget-datetimepicker',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@kartik/datetime' => $vendorDir . '/kartik-v/yii2-widget-datetimepicker',
    ),
  ),
  '2amigos/yii2-google-places-library' => 
  array (
    'name' => '2amigos/yii2-google-places-library',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@dosamigos/google/places' => $vendorDir . '/2amigos/yii2-google-places-library',
    ),
  ),
  'codemix/yii2-localeurls' => 
  array (
    'name' => 'codemix/yii2-localeurls',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@codemix/localeurls' => $vendorDir . '/codemix/yii2-localeurls',
    ),
  ),
  'yiisoft/yii2-bootstrap' => 
  array (
    'name' => 'yiisoft/yii2-bootstrap',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/bootstrap' => $vendorDir . '/yiisoft/yii2-bootstrap',
    ),
  ),
  'yiisoft/yii2-debug' => 
  array (
    'name' => 'yiisoft/yii2-debug',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/debug' => $vendorDir . '/yiisoft/yii2-debug',
    ),
  ),
  'yiisoft/yii2-gii' => 
  array (
    'name' => 'yiisoft/yii2-gii',
    'version' => '9999999-dev',
    'alias' => 
    array (
      '@yii/gii' => $vendorDir . '/yiisoft/yii2-gii',
    ),
  ),
);
