<?php

namespace common\models\query;

use yii\db\ActiveQuery;
use creocoder\taggable\TaggableQueryBehavior;

/**
 * This is the ActiveQuery class for [[\common\models\Sports]].
 *
 * @see \common\models\Sports
 */
class SpotQuery extends ActiveQuery
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
				TaggableQueryBehavior::className(),
		];
	}
	
}
