<?php

namespace common\models\query;

use yii\db\ActiveQuery;
use common\models\Sports;
use creocoder\taggable\TaggableQueryBehavior;

/**
 * This is the ActiveQuery class for [[\common\models\Sports]].
 *
 * @see \common\models\Sports
 */
class SportsQuery extends ActiveQuery
{
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
				TaggableQueryBehavior::className(),
		];
	}
	
    /**
     * @return $this
     */
    public function noParents()
    {
        $this->andWhere('{{%sports}}.parent_id IS NULL');

        return $this;
    }
    public function withParents()
    {
    	$this->andWhere('{{%sports}}.parent_id IS NOT NULL');
    
    	return $this;
    }
}
