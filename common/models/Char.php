<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "char".
 *
 * @property integer $id
 * @property string $name
 * @property integer $order
 * @property integer $status
 *
 * @property SpotChar[] $spotChars
 * @property Spot[] $spots
 */
class Char extends ActiveRecord
{
	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%char}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 200],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpotChars()
    {
        return $this->hasMany(SpotChar::className(), ['char_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpots()
    {
        return $this->hasMany(Spot::className(), ['id' => 'spot_id'])->viaTable('{{%spot_char}}', ['char_id' => 'id']);
    }
}
