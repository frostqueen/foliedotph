<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "ticket".
 *
 * @property integer $id
 * @property string $name
 * @property string $price
 * @property string $description
 * @property integer $limit
 * @property string $type
 * @property integer $dateStart
 * @property integer $dateEnd
 *
 * @property PurchaseTicket[] $purchaseTickets
 * @property SportsEvents[] $sportsEvents
 */
class Ticket extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%ticket}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price', 'description', 'limit', 'type', 'dateStart', 'dateEnd'], 'required'],
            [['price'], 'number'],
            [['description', 'type'], 'string'],
            [['limit', 'dateStart', 'dateEnd'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'price' => Yii::t('common', 'Price'),
            'description' => Yii::t('common', 'Description'),
            'limit' => Yii::t('common', 'Limit'),
            'type' => Yii::t('common', 'Type'),
            'dateStart' => Yii::t('common', 'Date Start'),
            'dateEnd' => Yii::t('common', 'Date End'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseTickets()
    {
        return $this->hasMany(PurchaseTicket::className(), ['ticket_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSportsEvents()
    {
        return $this->hasMany(SportsEvents::className(), ['ticket_id' => 'id']);
    }
}
