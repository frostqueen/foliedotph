<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "purchase".
 *
 * @property integer $id
 * @property string $date
 * @property string $total
 * @property string $mop
 * @property integer $count
 *
 * @property PurchaseTicket[] $purchaseTickets
 */
class Purchase extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%purchase}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'total', 'mop'], 'required'],
            [['date'], 'safe'],
            [['total'], 'number'],
            [['count'], 'integer'],
            [['mop'], 'string', 'max' => 45],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'date' => Yii::t('common', 'Date'),
            'total' => Yii::t('common', 'Total'),
            'mop' => Yii::t('common', 'Mop'),
            'count' => Yii::t('common', 'Count'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseTickets()
    {
        return $this->hasMany(PurchaseTicket::className(), ['purchase_id' => 'id']);
    }
}
