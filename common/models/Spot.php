<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use vova07\fileapi\behaviors\UploadBehavior;
use creocoder\taggable\TaggableBehavior;
use common\models\query\SpotQuery;

/**
 * This is the model class for table "{{%spot}}".
 *
 * @property integer $id
 * @property integer $city_id
 * @property string $avatar
 * @property string $description
 * @property string $address
 * @property integer $updated_at
 * @property integer $created_at
 * @property integer $status
 *
 * @property string $sportsValues
 * @property SportsSpot[] $sportsSpots
 * @property Sports[] $sports
 * @property City $city
 * @property SpotChar[] $spotChars
 * @property Char[] $chars
 */
class Spot extends ActiveRecord
{
	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%spot}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
    	return [
			[
	            'class' => TimestampBehavior::className(),
	            'createdAtAttribute' => 'created_at',
	            'updatedAtAttribute' => 'updated_at',
			],
    			'uploadBehavior' => [
    				'class' => UploadBehavior::className(),
    				'attributes' => [
    					'avatar' => [
    						'path' => '@storage/folie/spots',
    						'tempPath' => '@storage/tmp',
    						'url' => Yii::getAlias('@storageUrl/folie/spots'),
    					],
					],
    			],
    			TaggableBehavior::className(),
    	];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name','city_id','status','sportsValues'], 'required'],
            [['city_id', 'created_at', 'updated_at', 'status','sportsValues'], 'integer'],
            [['description'], 'string'],
        	[['name', 'avatar'], 'string', 'max' => 255],
            [['address'], 'string', 'max' => 500],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
        	[['status'], 'default', 'value' => self::STATUS_INACTIVE],
        	[['status'], 'in', 'range' => array_keys(self::statuses())],
        	['sportsValues', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
        	'name' => Yii::t('common', 'Name'),
        	'avatar' => Yii::t('common', 'Avatar'),
            'city_id' => Yii::t('common', 'City ID'),
            'description' => Yii::t('common', 'Description'),
            'address' => Yii::t('common', 'Address'),
            'created_at' => Yii::t('common', 'Created Date'),
            'updated_at' => Yii::t('common', 'Last Update'),
            'status' => Yii::t('common', 'Status'),
        	'sportsValues' => Yii::t('common', 'Sports'),
        ];
    }
    
    /**
     * @inheritdoc
     */
    public function transactions()
    {
    	return [
    			self::SCENARIO_DEFAULT => self::OP_ALL,
    	];
    }
    
    /**
     * Returns user statuses list
     *
     * @param mixed $status
     * @return array|mixed
     */
    public static function statuses($status = null)
    {
    	$statuses = [
    			self::STATUS_INACTIVE => Yii::t('common', 'Inactive'),
    			self::STATUS_ACTIVE => Yii::t('common', 'Active'),
    	];
    
    	if ($status === null) {
    		return $statuses;
    	}
    	return $statuses[$status];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSportsSpots()
    {
        return $this->hasMany(SportsSpot::className(), ['spot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSports()
    {
        return $this->hasMany(Sports::className(), ['id' => 'sports_id'])->viaTable('{{%sports_spot}}', ['spot_id' => 'id']);
    }
    
    /**
     * Returns tags that post is tagged with (as links).
     *
     * @return string
     */
    public function getSportsLinks()
    {
    	$sportsLinks = [];
    
    	foreach ($this->sports as $sports) {
    		$sportsLinks[] = Html::a($sports->name, ['sports', 'slug' => $sport->name]);
    	}
    
    	return implode(', ', $sportsLinks);
    }
    public function getSportLinks()
    {
    	$sportLinks = [];
    
    	foreach ($this->sports as $sport) {
    		$sportLinks[] = Html::a($sport->name, ['sport', 'slug' => $sport->name]);
    	}
    
    	return implode(', ', $sportLinks);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpotChars()
    {
        return $this->hasMany(SpotChar::className(), ['spot_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChars()
    {
        return $this->hasMany(Char::className(), ['id' => 'char_id'])
        	->viaTable('{{%spot_char}}', ['spot_id' => 'id'], function ($query) {
   		});
    }
    
    /**
     * @inheritdoc
     * @return \common\models\query\ArticleQuery the active query used by this AR class.
     */
    public static function find()
    {
    	return new SpotQuery(get_called_class());
    }
}
