<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "country".
 *
 * @property string $name
 * @property string $iso3
 * @property integer $status
 * @property State[] $states
 */
class Country extends ActiveRecord
{
	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%country}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'iso3'], 'required'],
            [['name'], 'string', 'max' => 100],
            [['iso3'], 'string', 'max' => 3],
            [['iso3'], 'unique'],
            [['name'], 'unique'],
        	[['status'], 'integer'],
       		[['status'], 'default', 'value' => self::STATUS_INACTIVE],
       		[['status'], 'in', 'range' => array_keys(self::statuses())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('common', 'Country'),
            'iso3' => Yii::t('common', 'Iso3'),
        	'staus' => Yii::t('common', 'Status'),
        ];
    }
    
    /**
     * Returns user statuses list
     *
     * @param mixed $status
     * @return array|mixed
     */
    public static function statuses($status = null)
    {
    	$statuses = [
    			self::STATUS_INACTIVE => Yii::t('common', 'Inactive'),
    			self::STATUS_ACTIVE => Yii::t('common', 'Active'),
    	];
    
    	if ($status === null) {
    		return $statuses;
    	}
    	return $statuses[$status];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStates()
    {
        return $this->hasMany(State::className(), ['country_name' => 'name']);
    }
}
