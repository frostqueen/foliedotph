<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use common\models\query\SportsQuery;
use vova07\fileapi\behaviors\UploadBehavior;

/**
 * This is the model class for table "sports".
 *
 * @property integer $id
 * @property string $name
 * @property string $avatar
 * @property string $description
 * @property integer $status
 * @property integer $parent_id
 *
 * @property EventtypeHasSports[] $eventtypeHasSports
 * @property Sports $parent
 * @property Sports[] $sports
 * @property SportsEvents[] $sportsEvents
 * @property SportsSpot[] $sportsSpots
 * @property Spot[] $spots
 */
class Sports extends ActiveRecord
{
	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%sports}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
    	return [
			'uploadBehavior' => [
    			'class' => UploadBehavior::className(),
    			'attributes' => [
    				'avatar' => [
    					'path' => '@storage/folie/sports',
    					'tempPath' => '@storage/tmp',
    					'url' => Yii::getAlias('@storageUrl/folie/sports'),
    					//'accept' => 'image/*',
    					//'maxSize' => 2000,
    				],
    			],
    		],
    	];
    }
    
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['status', 'parent_id'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['avatar'], 'string', 'max' => 255],
        	//[['avatar'], 'image', 'extensions'=>'svg, png'],
        	[['parent_id'], 'compare', 'compareAttribute' => 'id', 'operator' => '!==', 'message' => 'Cannot categorize to itself'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sports::className(), 'targetAttribute' => ['parent_id' => 'id']],
        	[['name'], 'safe'],
        	[['status'], 'default', 'value' => self::STATUS_INACTIVE],
        	[['status'], 'in', 'range' => array_keys(self::statuses())],
        	['frequency', 'default', 'value' => 0],
        	['frequency', 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'avatar' => Yii::t('common', 'Avatar'),
            'description' => Yii::t('common', 'Description'),
            'status' => Yii::t('common', 'Status'),
            'parent_id' => Yii::t('common', 'Sports Category'),
        	'frequency' => Yii::t('common', '# of Events'),
        ];
    }

    /**
     * Returns user statuses list
     *
     * @param mixed $status
     * @return array|mixed
     */
    public static function statuses($status = null)
    {
    	$statuses = [
    			self::STATUS_INACTIVE => Yii::t('common', 'Inactive'),
    			self::STATUS_ACTIVE => Yii::t('common', 'Active'),
    	];
    
    	if ($status === null) {
    		return $statuses;
    	}
    	return $statuses[$status];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(self::className(), ['id' => 'parent_id']);
    }
    
    public function getChilds()
    {
    	return $this->hasMany(self::className(), ['parent_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSports()
    {
        return $this->hasMany(Sports::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSportsEvents()
    {
        return $this->hasMany(SportsEvents::className(), ['sports_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSportsSpots()
    {
        return $this->hasMany(SportsSpot::className(), ['sports_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpots()
    {
        return $this->hasMany(Spot::className(), ['id' => 'spot_id'])->viaTable('{{%sports_spot}}', ['sports_id' => 'id']);
    }
    
    /**
     * @inheritdoc
     * @return \common\models\query\SportsQuery the active query used by this AR class.
     */
    public static function find()
    {
    	return new SportsQuery(get_called_class());
    }
    
}
