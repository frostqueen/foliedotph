<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "state".
 *
 * @property integer $id
 * @property string $name
 * @property string $country_name
 * @property integer $status
 * @property City[] $cities
 * @property Country $countryName
 */
class State extends ActiveRecord
{
	const STATUS_INACTIVE = 0;
	const STATUS_ACTIVE = 1;
	
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%state}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'country_name'], 'required'],
            [['country_name'], 'string', 'max' => 100],
            [['name'], 'string', 'max' => 255],
            [['country_name'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_name' => 'name']],
        	[['country_name'], 'safe'],
        	[['status'], 'integer'],
        	[['status'], 'default', 'value' => self::STATUS_INACTIVE],
        	[['status'], 'in', 'range' => array_keys(self::statuses())],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Province'),
            'country_name' => Yii::t('common', 'Country Name'),
        	'status' => Yii::t('common', 'Status'),
        ];
    }
    
    /**
     * Returns user statuses list
     *
     * @param mixed $status
     * @return array|mixed
     */
    public static function statuses($status = null)
    {
    	$statuses = [
    			self::STATUS_INACTIVE => Yii::t('common', 'Inactive'),
    			self::STATUS_ACTIVE => Yii::t('common', 'Active'),
    	];
    
    	if ($status === null) {
    		return $statuses;
    	}
    	return $statuses[$status];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCities()
    {
        return $this->hasMany(City::className(), ['state_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountryName()
    {
        return $this->hasOne(Country::className(), ['name' => 'country_name']);
    }
}
