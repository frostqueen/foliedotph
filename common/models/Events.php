<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use vova07\fileapi\behaviors\UploadBehavior;
use creocoder\taggable\TaggableBehavior;

/**
 * This is the model class for table "events".
 *
 * @property integer $id
 * @property string $name
 * @property string $avatar
 * @property string $dateStart
 * @property string $dateEnd
 * @property string $address
 * @property integer $city_id
 * @property integer $state_id
 * @property integer $country_name
 * @property string $description
 * @property integer $ticketLimit
 * @property integer $ticketCount
 * @property string $thingsToBring
 * @property string $reminders
 * @property string $inclusions
 * @property string $exclusions
 *
 * @property City $city
 * @property SportsEvents[] $sportsEvents
 */
class Events extends ActiveRecord
{
	const STATUS_DRAFT = 0;
	const STATUS_PUBLISH = 1;
	
    public static function tableName()
    {
        return '{{%events}}';
    }
    
    public function behaviors()
    {
    	return [
    		'uploadBehavior' => [
    			'class' => UploadBehavior::className(),
    				'attributes' => [
    					'avatar' => [
    						'path' => '@storage/folie/events',
    						'tempPath' => '@storage/tmp',
    						'url' => Yii::getAlias('@storageUrl/folie/events'),
    					],
    				],
    		],
    		TaggableBehavior::className(),
    	];
    }

    public function rules()
    {
        return [
            [['name', 'dateStart', 'dateEnd', 'address', 'city_id', 'state_id', 'country_name', 'description', 'ticketLimit'], 'required'],
            [['dateStart', 'dateEnd'], 'safe'],
        	[['dateStart', 'dateEnd'], 'default',
        		'value' => function () {
        			return date(DATE_ISO8601);
        		}
        	],
        	//['avatar', 'image', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 10000, 'tooBig' => 'Limit is 100KB'],
        	['dateStart','validateDates'],
        	['ticketCount', 'default', 'value' => self::STATUS_DRAFT],
        	[['dateStart', 'dateEnd'], 'filter', 'filter' => 'strtotime'],
            [['city_id', 'state_id', 'ticketLimit', 'ticketCount'], 'integer'],
            [['description', 'thingsToBring', 'reminders', 'inclusions', 'exclusions'], 'string'],
            [['name', 'avatar'], 'string', 'max' => 255],
            [['address'], 'string', 'max' => 500],
            [['country_name'], 'string', 'max' => 100],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['state_id'], 'exist', 'skipOnError' => true, 'targetClass' => State::className(), 'targetAttribute' => ['state_id' => 'id']],
            [['country_name'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_name' => 'name']],
            ['tagValues', 'safe'],
        ];
    }
    
    public function validateDates(){
    	if(strtotime($this->dateEnd) <= strtotime($this->dateStart)){
    		$this->addError('dateStart','dateStart must be lower than dateEnd');
    		$this->addError('dateEnd','dateEnd must be greater than dateStart');
    	}
    }

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('common', 'ID'),
            'name' => Yii::t('common', 'Name'),
            'avatar' => Yii::t('common', 'Avatar'),
            'dateStart' => Yii::t('common', 'Date Start'),
            'dateEnd' => Yii::t('common', 'Date End'),
            'address' => Yii::t('common', 'Address'),
            'city_id' => Yii::t('common', 'City ID'),
        	'state_id' => Yii::t('common', 'State ID'),
        	'country_name' => Yii::t('common', 'Country Name'),
            'description' => Yii::t('common', 'Description'),
            'ticketLimit' => Yii::t('common', 'Ticket Limit'),
            'ticketCount' => Yii::t('common', 'Show remaining ticket(s)'),
            'thingsToBring' => Yii::t('common', 'Things To Bring'),
            'reminders' => Yii::t('common', 'Reminders'),
            'inclusions' => Yii::t('common', 'Inclusions'),
            'exclusions' => Yii::t('common', 'Exclusions'),
        ];
    }
    
    /**
     * Returns user statuses list
     *
     * @param mixed $status
     * @return array|mixed
     */
    public static function statuses($status = null)
    {
    	$statuses = [
    			self::STATUS_DRAFT => Yii::t('common', 'Draft'),
    			self::STATUS_PUBLISH => Yii::t('common', 'Publish'),
    	];
    
    	if ($status === null) {
    		return $statuses;
    	}
    	return $statuses[$status];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getState()
    {
    	return $this->hasOne(State::className(), ['id' => 'state_id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
    	return $this->hasOne(Country::className(), ['name' => 'country_name']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSportsEvents()
    {
        return $this->hasMany(SportsEvents::className(), ['events_id' => 'id']);
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
    	return $this->hasMany(Sports::className(), ['id' => 'sports_id'])->viaTable('{{%sports_events}}', ['events_id' => 'id']);
    }
    
    public function getTagLinks()
    {
    	$tagLinks = [];
    
    	foreach ($this->tags as $tag) {
    		$tagLinks[] = Html::a($tag->name, ['tag', 'slug' => $tag->slug]);
    	}
    
    	return implode(', ', $tagLinks);
    }
}
