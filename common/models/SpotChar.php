<?php

namespace common\models;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "spot_char".
 *
 * @property integer $spot_id
 * @property integer $char_id
 *
 * @property Char $char
 * @property Spot $spot
 */
class SpotChar extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%spot_char}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['char_id'], 'required'],
            [['char_id'], 'integer'],
            [['char_id'], 'exist', 'skipOnError' => true, 'targetClass' => Char::className(), 'targetAttribute' => ['char_id' => 'id']],
            [['spot_id'], 'exist', 'skipOnError' => true, 'targetClass' => Spot::className(), 'targetAttribute' => ['spot_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'spot_id' => Yii::t('backend', 'Spot ID'),
            'char_id' => Yii::t('backend', 'Char ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChar()
    {
        return $this->hasOne(Char::className(), ['id' => 'char_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpot()
    {
        return $this->hasOne(Spot::className(), ['id' => 'spot_id']);
    }
}
