<?php

$config = [
    'name'=>'Folie',
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'extensions' => require(__DIR__ . '/../../vendor/yiisoft/extensions.php'),
    'sourceLanguage' => 'en-US',
    'language' => getenv('LANGUAGE'),
    'bootstrap' => ['log'],
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => getenv('DB_DSN'),
            'username' => getenv('DB_USERNAME'),
            'password' => getenv('DB_PASSWORD'),
            'tablePrefix' => getenv('DB_TABLE_PREFIX'),
            'charset' => 'utf8',
            'enableSchemaCache' => YII_ENV_PROD,
        ],
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'assetManager' => [
            'class' => 'yii\web\AssetManager',
            'linkAssets' => true,
            'appendTimestamp' => YII_ENV_DEV,
        	/*'bundles' => [
        		'dosamigos\google\maps\MapAsset' => [
        			'options' => [
        			'key' => 'AIzaSyDSVLlJY_gi3lu4sCXZYnMd5p-yjYx4P0g',
        			'language' => 'en',
        			'version' => '3.1.18'
        			]
        		]
        	]*/
        ],
        'log' => [
            'traceLevel' => YII_ENV_DEV ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\DbTarget',
                    'levels' => ['error', 'warning'],
                    'except' => ['yii\web\HttpException:*', 'yii\i18n\I18N\*'],
                    'prefix' => function () {
                        $url = !Yii::$app->request->isConsoleRequest ? Yii::$app->request->getUrl() : null;

                        return sprintf('[%s][%s]', Yii::$app->id, $url);
                    },
                    'logVars' => [],
                ],
            ],
        ],
        'i18n' => [
            'translations' => [
                'app' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                ],
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@common/messages',
                    'fileMap' => [
                        'common' => 'common.php',
                        'backend' => 'backend.php',
                        'frontend' => 'frontend.php',
                    ],
                ],
            ],
        ],
        'formatter' => [
        		'dateFormat' => 'medium',
        		//'datetimeFormat' => 'M dd yyyy - hh:ss P',
        		'timeFormat' => 'H:m',
        ],
        'keyStorage' => [
            'class' => 'common\components\keyStorage\KeyStorage',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => YII_ENV_DEV,
        ],
        'cache' => [
            'class' => YII_ENV_DEV ? 'yii\caching\DummyCache' : 'yii\caching\FileCache',
        ],
    ],
    'modules' => [
    	'noty' => [
    		'class' => 'lo\modules\noty\Module',
    	],
    	'social' => [
    		'class' => 'kartik\social\Module',
    		'disqus' => [
    			'settings' => ['shortname' => getenv('SHORT_NAME')]
    		],
    		'facebook' => [
    			'appId' => '1517638078535148',
    			'secret' => 'bfb7db32da1092d4750de44cf0a43989',
    		],	
    		/*'google' => [
    			'clientId' => 'GOOGLE_API_CLIENT_ID',
    			'pageId' => '101488895635263675220',
    			'profileId' => '+MJMitraOfficial',
    		],*/
    		'googleAnalytics' => [
    			'id' => 'TRACKING_ID',
    			'domain' => 'TRACKING_DOMAIN',
    		], 		
    		'twitter' => [
    			'screenName' => '@FolieDotPH'
    		],
    	],
    ],
];

return $config;
